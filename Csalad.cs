﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Lab8
{
    public struct OneClass2
    {
        public string csaladnev;
        public int tagokszama;
        public int allamok;
        public int karosodas;
        public int kirandulas;
        public int csapat;
    }
    public class Csalad : DAL
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Csalad(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "Csaladok");
        }

        public int alakit2()
        {
            string error2 = "";
            SqlDataReader olvaso = myExecuteReader("SELECT AllamID FROM Allamok Where AllamNev = comboBox1.Text", ref error2);
            int x = Convert.ToInt32(olvaso);
            return x;
        }
        public List<OneClass2> kiolvas()
        {
            List<OneClass2> Csaladok = new List<OneClass2>();
            OneClass2 oc = new OneClass2();
            string error = "";
            SqlDataReader reader = myExecuteReader("SELECT CsaladNev, TagokSzama, Allamok.AllamID, Karosodasok.KarosodasID, Kirandulasok.KirandulasID, TervezoCsapatok.CsapatID FROM Csaladok, Allamok, Karosodasok, TervezoCsapatok, Kirandulasok WHERE TervezoCsapatok.CsapatID=Csaladok.CsapatID and Csaladok.AllamID = Allamok.AllamID and Csaladok.KirandulasID = Kirandulasok.KirandulasID and Csaladok.KarosodasID = Karosodasok.KarosodasID and CsapatNev like", ref error);
            while (reader.Read())
            {
                oc.csaladnev = reader["CsaladNev"].ToString();
                oc.tagokszama = Convert.ToInt32(reader["TagokSzama"]);
                oc.allamok = Convert.ToInt32(reader["AllamID"]);
                oc.karosodas = Convert.ToInt32(reader["KarosodasID"]);
                oc.kirandulas = Convert.ToInt32(reader["KirandulasID"]);
                oc.csapat = Convert.ToInt32(reader["CsapatNev"]);
                Csaladok.Add(oc);
            }
            return Csaladok;
        }
        public DataSet kiolvas_ds(ref string Errm)
        {
            string s = "SELECT CsaladID, CsaladNev, TagokSzama, Allamok.AllamNev, Karosodasok.KarosodasNev, Kirandulasok.KirandulasNev, TervezoCsapatok.CsapatNev FROM Csaladok, Allamok, Karosodasok, TervezoCsapatok, Kirandulasok WHERE TervezoCsapatok.CsapatID=Csaladok.CsapatID and Csaladok.AllamID = Allamok.AllamID and Csaladok.KirandulasID = Kirandulasok.KirandulasID and Csaladok.KarosodasID = Karosodasok.KarosodasID";
            ds = base.myFillDataSetFromString(s, "Csaladok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_tag(ref string Errm)
        {//tagok kiolvasasa
            string s = "SELECT TagID, TagNev, CsaladNev FROM Tagok, Csaladok WHERE Tagok.CsaladID = Csaladok.CsaladID";
            ds = base.myFillDataSetFromString(s, "Csaladok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds1(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsaladNev, TagokSzama, Allamok.AllamID, Karosodasok.KarosodasID, Kirandulasok.KirandulasID, TervezoCsapatok.CsapatID FROM Csaladok, Allamok, Karosodasok, TervezoCsapatok, Kirandulasok WHERE TervezoCsapatok.CsapatID=Csaladok.CsapatID and Csaladok.AllamID = Allamok.AllamID and Csaladok.KirandulasID = Kirandulasok.KirandulasID and Csaladok.KarosodasID = Karosodasok.KarosodasID and AllamNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Csaladok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds10(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsaladNev FROM Csaladok, Kirandulasok WHERE Csaladok.KirandulasID = Kirandulasok.KirandulasID and KirandulasNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Csaladok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds2(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsaladNev, TagokSzama, Allamok.AllamID, Karosodasok.KarosodasID, Kirandulasok.KirandulasID, TervezoCsapatok.CsapatID FROM Csaladok, Allamok, Karosodasok, TervezoCsapatok, Kirandulasok WHERE TervezoCsapatok.CsapatID=Csaladok.CsapatID and Csaladok.AllamID = Allamok.AllamID and Csaladok.KirandulasID = Kirandulasok.KirandulasID and Csaladok.KarosodasID = Karosodasok.KarosodasID and KarosodasNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Csaladok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds3(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsaladNev, TagokSzama, Allamok.AllamID, Karosodasok.KarosodasID, Kirandulasok.KirandulasID, TervezoCsapatok.CsapatID FROM Csaladok, Allamok, Karosodasok, TervezoCsapatok, Kirandulasok WHERE TervezoCsapatok.CsapatID=Csaladok.CsapatID and Csaladok.AllamID = Allamok.AllamID and Csaladok.KirandulasID = Kirandulasok.KirandulasID and Csaladok.KarosodasID = Karosodasok.KarosodasID and KirandulasNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Csaladok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds4(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsaladNev, TagokSzama, Allamok.AllamID, Karosodasok.KarosodasID, Kirandulasok.KirandulasID, TervezoCsapatok.CsapatID FROM Csaladok, Allamok, Karosodasok, TervezoCsapatok, Kirandulasok WHERE TervezoCsapatok.CsapatID=Csaladok.CsapatID and Csaladok.AllamID = Allamok.AllamID and Csaladok.KirandulasID = Kirandulasok.KirandulasID and Csaladok.KarosodasID = Karosodasok.KarosodasID and CsapatNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Csaladok", ref da, ref Errm);
            return ds;
        }
        public void beszuras(string csaladnev, string tagokszama, string allamnev, string karosodasnev, string kirandulas, string csapatnev)
        {
            OpenConnection();
            //beszurunk egy uj sort az Csaladok tablaba
            string Err = "";
            sQuery = "SELECT AllamID FROM Allamok WHERE AllamNev = '" + allamnev + "'";
            int allamid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT KarosodasID FROM Karosodasok WHERE KarosodasNev = '" + karosodasnev + "'";
            int karosodasid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT KirandulasID FROM Kirandulasok WHERE KirandulasNev = '" + kirandulas + "'";
            int kirandulasid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT CsapatID FROM TervezoCsapatok WHERE CsapatNev = '" + csapatnev + "'";
            int csapatid = base.myExecuteScalar(sQuery, ref Err);

            string s = "INSERT INTO Csaladok (CsaladNev, TagokSzama, AllamID, KarosodasID, KirandulasID, CsapatID) VALUES ('" + csaladnev + "'," + tagokszama + "," + allamid + "," + karosodasid + "," + kirandulasid + "," + csapatid + ")";
         
            base.myInsertCommand(s);
            CloseConnection();
        }

        public void modosit(int ID, String csaladnev, String tagokszama, String allam, String karosodas, String csapat, String kirandulas )
        {
            OpenConnection();
            string Err = "";
            sQuery = "SELECT AllamID FROM Allamok WHERE AllamNev = '" + allam + "'";
            int allamid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT KarosodasID FROM Karosodasok WHERE KarosodasNev = '" + karosodas + "'";
            int karosodasid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT KirandulasID FROM Kirandulasok WHERE KirandulasNev = '" + kirandulas + "'";
            int kirandulasid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT CsapatID FROM TervezoCsapatok WHERE CsapatNev = '" + csapat + "'";
            int csapatid = base.myExecuteScalar(sQuery, ref Err);

            string s = "UPDATE Csaladok SET CsaladNev = '" + csaladnev+ "',TagokSzama = " + tagokszama +",AllamID = " + allamid +",KarosodasID = " + karosodasid +",KirandulasID = " + kirandulasid +",CsapatID = " + csapatid +" WHERE CsaladID = " + ID;
         
            base.myInsertCommand(s);
            CloseConnection();
        }

    }
}