﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Lab8
{
    class Tervezok : DAL
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Tervezok(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "Tervezok");
        }

        public DataSet kiolvas_ds(string sBetuk, ref string Errm)
        {
            string s = "SELECT TervezNev FROM Tervezok, Tervez, TervezoCsapatok WHERE Tervez.TervezoID = Tervezok.TervezoID and Tervez.CsapatID = TervezoCsapatok.CsapatID and CsapatNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Onkentesek", ref da, ref Errm);
            return ds;
        }


    }
}
