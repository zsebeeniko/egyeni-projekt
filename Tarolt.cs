﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Lab8
{//Tarolt eljarasok
    class Tarolt : DAL
    {
        SqlDataAdapter da = new SqlDataAdapter();
        SqlDataAdapter da1 = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Tarolt(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "Tervezok");
           da1.TableMappings.Add("Table", "tam");
        }

        public String procedure1(string o, ref string Errm)
        {
            try
            {
                mySqlConn.Open();
                SqlCommand cmd = new SqlCommand("kirandul", mySqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@kirandulas", SqlDbType.VarChar, 30).Value = o;
                cmd.Parameters.Add("@eredmeny", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                String kirandul_value = cmd.Parameters["@eredmeny"].Value.ToString();
                mySqlConn.Close();
                return kirandul_value;
            }
            catch(Exception ex)
            {
                String Errm3 = "";
                Errm3 = ex.Message;
                return null;
            }
        }

        public String procedure2(string o, ref string Errm)
        {
            try
            {
                mySqlConn.Open();
                SqlCommand cmd = new SqlCommand("hanybeteg", mySqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@betegseg", SqlDbType.VarChar, 30).Value = o;
                cmd.Parameters.Add("@eredmeny", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                String beteg_value = cmd.Parameters["@eredmeny"].Value.ToString();
                mySqlConn.Close();
                return beteg_value;
            }
            catch (Exception ex)
            {
                String Errm3 = "";
                Errm3 = ex.Message;
                return null;
            }
        }
        public String procedure3(ref string Errm)
        {
            try
            {
                mySqlConn.Open();
                SqlCommand cmd = new SqlCommand("osszonkentes", mySqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@eredmeny", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                String onkentes_value = cmd.Parameters["@eredmeny"].Value.ToString();
                mySqlConn.Close();
                return onkentes_value;
            }
            catch (Exception ex)
            {
                String Errm3 = "";
                Errm3 = ex.Message;
                return null;
            }
        }

        public String procedure4(ref string Errm)
        {
            try
            {
                mySqlConn.Open();
                SqlCommand cmd = new SqlCommand("tamogatas", mySqlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@eredmeny", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                String tamogat_value = cmd.Parameters["@eredmeny"].Value.ToString();
                mySqlConn.Close();
                return tamogat_value;
            }
            catch( Exception ex)
            {
                String Errm3 = "";
                Errm3 = ex.Message;
                return null;
            }
        }
        public DataSet procedure5(int tamogat, ref string Errm)
        {
            try
            {
                OpenConnection();
                myComm = new SqlCommand("tamogatott", mySqlConn);
                SqlParameter tam = new SqlParameter("@tam", SqlDbType.Int);
                tam.Direction = ParameterDirection.Input;
                myComm.Parameters.Add("@tam", SqlDbType.Int).Value = tamogat;    
                myComm.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand = myComm;
                DataSet ds = new DataSet("tamog");
                da1.Fill(ds);
                CloseConnection();
                Errm = "OK";
                return ds;
            }
            catch (Exception ex)
            {
                Errm = ex.Message;
                return null;
            }

        }
        public DataSet procedure6(int tamogat, ref string Errm)
        {
            try
            {
                OpenConnection();
                myComm = new SqlCommand("tamogatott2", mySqlConn);
                SqlParameter tam = new SqlParameter("@tam", SqlDbType.Int);
                tam.Direction = ParameterDirection.Input;
                myComm.Parameters.Add("@tam", SqlDbType.Int).Value = tamogat;
                myComm.CommandType = CommandType.StoredProcedure;
                da1.SelectCommand = myComm;
                DataSet ds = new DataSet("tamog");
                da1.Fill(ds);
                CloseConnection();
                Errm = "OK";
                return ds;
            }
            catch (Exception ex)
            {
                Errm = ex.Message;
                return null;
            }

        }
    }
}
