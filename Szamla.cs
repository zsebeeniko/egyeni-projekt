﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Lab8
{
    public partial class Szamla : Form
    {
        public bool Err;
        String ConnectionString = "Data Source=(local);Initial Catalog=projekt;Integrated Security=SSPI";
        public ArrayList al;
        TCsapat tc = new TCsapat();

        public ArrayList al2;
        Tamogatok t = new Tamogatok();

        System.Data.DataSet ds;

        public Szamla()
        {
            InitializeComponent();
        }

        private void Form10_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Chocolate; //a form szine
            dataGridView1.BackgroundColor = System.Drawing.Color.Beige; //a datagridview-k szine
            //dataGridView2.BackgroundColor = System.Drawing.Color.Beige;
            al = new ArrayList();
            al = tc.Feltolt();
            //comboBoxok feltoltese
            foreach (string c in al)
            {
                comboBox1.Items.Add(c);
            }
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            al2 = new ArrayList();
            al2 = t.Feltolt();
            //comboBoxok feltoltese
            foreach (string c in al2)
            {
                comboBox2.Items.Add(c);
            }
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast
            
            if (Globalis.var == 3)
            {// ha a belepo latogato
                button1.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Szamlazas
            string datum1 = textBox1.Text.ToString();
            string datum2 = textBox2.Text.ToString();
            string datum = textBox4.Text.ToString();
            string tamogat = comboBox2.SelectedItem.ToString();
            string csapat = comboBox1.SelectedItem.ToString();
            int osszeg = Convert.ToInt32(textBox3.Text);
            int osszeg_vegso = osszeg * 124/100;
            string ErrM = "";         
            Tamogatok2 t2 = new Tamogatok2(ref Err);
            int tamogatoid = t2.tamogatoid(ref ErrM, tamogat);
            int csapatid = t2.csapatid(ref ErrM, csapat);
 
            SqlConnection connection = new SqlConnection(ConnectionString);
            connection.Open();

                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction(IsolationLevel.Serializable);

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;
                try
                {
                    command.CommandText =
                        "INSERT into Szamlak Values(" + csapatid + "," + tamogatoid + "," + osszeg_vegso + ", '"+ datum1 +"','" + datum2 + "', '" + datum + "')";
                    command.ExecuteNonQuery();
                    System.Threading.Thread.Sleep(1000);

                    command.CommandText = "SELECT SzamlaID from Szamlak where Datum_szamla = '" + datum + "' and CsapatID =" + csapatid + " and TamogatoID = " + tamogatoid + " and Osszeg = " + osszeg_vegso + "and  Datum_kezdo ='" + datum1 + "' and Datum_vegso = '" + datum2 + "'";
                    int szamlaid = Convert.ToInt32(command.ExecuteScalar());
                    label9.Text = szamlaid.ToString();

                    foreach (DataGridViewRow sor in dataGridView1.Rows)
                    {
                        if (sor.Cells.ToString() != String.Empty)
                        {
                            int osszeg_szamla = Convert.ToInt32(sor.Cells[0].Value);
                            if (osszeg_szamla != 0)
                            {
                                string datum_szamla = sor.Cells[1].Value.ToString();

                                command.CommandText =
                                   "INSERT into SzamlaSorok Values(" + szamlaid + "," + osszeg_szamla + ",'" + datum_szamla + "')";
                                command.ExecuteNonQuery();
                            }
                            else {
                                MessageBox.Show("Select a non null row");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Select a non null row");
                        }
                    }
                    // Attempt to commit the transaction.
                    transaction.Commit();
                    MessageBox.Show("A szamla sikeresen letrehozva !!!");
                }
                catch (Exception EX)
                {
                    Console.WriteLine("Commit Exception Type: {0}", EX.GetType());
                    Console.WriteLine("  Message: {0}", EX.Message);

                    // Attempt to roll back the transaction. 
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // This catch block will handle any errors that may have occurred 
                        // on the server that would cause the rollback to fail, such as 
                        // a closed connection.
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Kilepes
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Listazas
            Tamogatok2 t2 = new Tamogatok2(ref Err);
            string ErrM = "";
            string datum1 = textBox1.Text.ToString();
            string datum2 = textBox2.Text.ToString();
            string csapatnev = comboBox1.SelectedItem.ToString();
            string tamogato = comboBox2.SelectedItem.ToString();
            
            ds = t2.kiolvas_tamogat(ref ErrM, datum1, datum2, csapatnev, tamogato);
            int osszeg = t2.Osszeg_szamla(ref ErrM, datum1, datum2, csapatnev, tamogato);
            
            textBox3.Text = Convert.ToString(osszeg);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Tamogat";
        }
    }
}
