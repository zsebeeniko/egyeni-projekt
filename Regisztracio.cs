﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Security.Cryptography;

namespace Lab8
{
    class Regisztracio : DAL
    {
         SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Regisztracio(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "Felhasznalok");
        }

        public int letezike(string user)
        {// ellenorzom a felhasznalonevet
            OpenConnection();
            int csopid = 0;
            string Err = "";
            sQuery = "SELECT FelhCsopID FROM Felhasznalok WHERE UserID = '" + user + "'";
            csopid = base.myExecuteScalar(sQuery, ref Err);
            return csopid;
        }
        
        public int letezike2(string user, string jelszo)
        {// ellenorzom a jelszot es a felhasznalonevet is
            OpenConnection();
            int csopid = 0;
            string Err = "";
            sQuery = "SELECT FelhCsopID FROM Felhasznalok WHERE UserID = '" + user + "' and Jelszo = '" + jelszo + "'";
            csopid = base.myExecuteScalar(sQuery, ref Err);
            return csopid;
        }
        
        public void regisztralas(string teljes, string user, string jelszo, string tipus)
        {
            OpenConnection();
            //beszurunk egy uj sort a Felhasznalok tablaba
            string Err = "";
            sQuery = "SELECT FelhCsopID FROM FelhCsoportok WHERE Tipus = '" + tipus + "'";
            int felhcsopid = base.myExecuteScalar(sQuery, ref Err);

            string s = "INSERT INTO Felhasznalok (UserID, Jelszo, FelhasznaloNev, FelhCsopID) VALUES ('" + user + "','" + jelszo + "','" + teljes + "'," + felhcsopid + ")";

            base.myInsertCommand(s);
            CloseConnection();

        }

        public string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }

}
