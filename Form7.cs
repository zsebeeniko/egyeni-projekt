﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8
{
    public partial class Form7 : Form
    {
        String st = "Data Source=(local);Initial Catalog=projekt;Integrated Security=SSPI";
        public bool Err;
        System.Data.DataSet ds;
        int tamogato_id1;
        int csapat_id1;
        int osszeg1;
        int osszeg1_modositott;
        int tamogato_id2;
        int csapat_id2;
        int osszeg2;
        int osszeg2_modositott;
      
        public Form7()
        {
            InitializeComponent();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Chocolate;
            dataGridView1.BackgroundColor = System.Drawing.Color.Beige; //hatterszin
            //button2.Enabled = false;
            //button4.Enabled = false;
            Tamogat_pesszimista b = new Tamogat_pesszimista(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds(ref ErrM);
            dataGridView1.DataSource = ds;  //DataGridView feltoltese
            dataGridView1.DataMember = "Tamogat";
            if (Globalis.var == 3)
            {
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                button6.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {// elso modositas
            try //hibakezeles
            {
                DataGridViewRow sor1 = dataGridView1.SelectedRows[0]; //egy sor a datagridview bol
                int value = Convert.ToInt32(sor1.Cells[2].Value);
                textBox1.Text = value.ToString();
                tamogato_id1 = Convert.ToInt32(sor1.Cells[0].Value);
                csapat_id1 = Convert.ToInt32(sor1.Cells[1].Value);
                osszeg1 = value;
            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
            //button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {// elso tranzakcio
           
            Tamogat_pesszimista tp = new Tamogat_pesszimista(ref Err);
            try
            {
                tp.Tranzakcio1(st, tamogato_id1, csapat_id1, osszeg1_modositott, tamogato_id2, csapat_id2, osszeg2_modositott);
            }
            catch(Exception Ex)
            {
                String Errm = "";
                Errm = Ex.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {// masodik modositas
            try //hibakezeles
            {
                DataGridViewRow sor1 = dataGridView1.SelectedRows[0]; //egy sor a datagridview bol
                int value = Convert.ToInt32(sor1.Cells[2].Value);
                textBox2.Text = value.ToString();
                tamogato_id2 = Convert.ToInt32(sor1.Cells[0].Value);
                csapat_id2 = Convert.ToInt32(sor1.Cells[1].Value);
                osszeg2 = value;
            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
            //button4.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {// masodik tranzakcio
           
            Tamogat_pesszimista tp = new Tamogat_pesszimista(ref Err);
            try
            {
                tp.Tranzakcio2(st, tamogato_id2, csapat_id2, osszeg2_modositott, tamogato_id1, csapat_id1, osszeg1_modositott);
            }
            catch (Exception Ex)
            {
                String Errm = "";
                Errm = Ex.Message;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {// kilepes
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {// mentes
            try
            {
                osszeg1_modositott = Convert.ToInt32(textBox1.Text);
                osszeg2_modositott = Convert.ToInt32(textBox2.Text);
            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
        }

    }
}
