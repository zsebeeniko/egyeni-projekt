﻿namespace Lab8
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_uj = new System.Windows.Forms.Button();
            this.but_modosit = new System.Windows.Forms.Button();
            this.but_torol = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.but_ment = new System.Windows.Forms.Button();
            this.but_megse = new System.Windows.Forms.Button();
            this.but_kilep = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // but_uj
            // 
            this.but_uj.Location = new System.Drawing.Point(30, 285);
            this.but_uj.Name = "but_uj";
            this.but_uj.Size = new System.Drawing.Size(75, 23);
            this.but_uj.TabIndex = 0;
            this.but_uj.Text = "Uj";
            this.but_uj.UseVisualStyleBackColor = true;
            this.but_uj.Click += new System.EventHandler(this.button1_Click);
            // 
            // but_modosit
            // 
            this.but_modosit.Location = new System.Drawing.Point(139, 285);
            this.but_modosit.Name = "but_modosit";
            this.but_modosit.Size = new System.Drawing.Size(75, 23);
            this.but_modosit.TabIndex = 1;
            this.but_modosit.Text = "Modosit";
            this.but_modosit.UseVisualStyleBackColor = true;
            this.but_modosit.Click += new System.EventHandler(this.button2_Click);
            // 
            // but_torol
            // 
            this.but_torol.Location = new System.Drawing.Point(250, 285);
            this.but_torol.Name = "but_torol";
            this.but_torol.Size = new System.Drawing.Size(75, 23);
            this.but_torol.TabIndex = 2;
            this.but_torol.Text = "Torol";
            this.but_torol.UseVisualStyleBackColor = true;
            this.but_torol.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(30, 35);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(444, 161);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(573, 100);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(573, 180);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 5;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(573, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(506, 183);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Beszallito:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(506, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Partner:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(506, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Csapatnev:";
            // 
            // but_ment
            // 
            this.but_ment.Location = new System.Drawing.Point(371, 285);
            this.but_ment.Name = "but_ment";
            this.but_ment.Size = new System.Drawing.Size(75, 23);
            this.but_ment.TabIndex = 10;
            this.but_ment.Text = "Mentes";
            this.but_ment.UseVisualStyleBackColor = true;
            this.but_ment.Click += new System.EventHandler(this.button4_Click);
            // 
            // but_megse
            // 
            this.but_megse.Location = new System.Drawing.Point(485, 285);
            this.but_megse.Name = "but_megse";
            this.but_megse.Size = new System.Drawing.Size(75, 23);
            this.but_megse.TabIndex = 11;
            this.but_megse.Text = "Megse";
            this.but_megse.UseVisualStyleBackColor = true;
            this.but_megse.Click += new System.EventHandler(this.button5_Click);
            // 
            // but_kilep
            // 
            this.but_kilep.Location = new System.Drawing.Point(603, 285);
            this.but_kilep.Name = "but_kilep";
            this.but_kilep.Size = new System.Drawing.Size(75, 23);
            this.but_kilep.TabIndex = 12;
            this.but_kilep.Text = "Kilepes";
            this.but_kilep.UseVisualStyleBackColor = true;
            this.but_kilep.Click += new System.EventHandler(this.but_kilep_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 375);
            this.Controls.Add(this.but_kilep);
            this.Controls.Add(this.but_megse);
            this.Controls.Add(this.but_ment);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.but_torol);
            this.Controls.Add(this.but_modosit);
            this.Controls.Add(this.but_uj);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_uj;
        private System.Windows.Forms.Button but_modosit;
        private System.Windows.Forms.Button but_torol;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button but_ment;
        private System.Windows.Forms.Button but_megse;
        private System.Windows.Forms.Button but_kilep;
    }
}

