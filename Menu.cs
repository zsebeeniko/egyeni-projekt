﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8
{
    public partial class Menu : Form
    {
        private System.Data.DataSet dsTagok;
        private bool Err = true;
        private XML_tablak myXML_tablak;
        private string ErrMess="";
        
        public Menu()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Bisque; //a form szine
        }

        private void menu1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close(); // Kilepes
        }

        private void tervezoCsapatokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1(); //TervezoCsapatok
            form.Show();
        }

        private void csaladokToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 form = new Form3(); //Csaladok
            form.Show();
        }

        private void szuresekToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 form = new Form4(); //Szuresek
            form.Show();
        }

        private void tamogatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form5 form = new Form5(); //Tamogat tabla sok a sokhoz
            form.Show();
        }

        private void konkurenciavezerlesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tranz_Opt Form = new Tranz_Opt();
            Form.Show();
        }

        private void pesszimistaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form7 form = new Form7();
            form.Show();
        }

        private void regisztracioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form8 form = new Form8();
            form.Show();
        }

        private void szamlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Szamla form = new Szamla();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // XML kigeneralasa....
            myXML_tablak = new XML_tablak(ref Err);
            dsTagok = myXML_tablak.XML_general(ref ErrMess);

            dsTagok.WriteXml("E:\\AAAgyetem\\4. felev\\Adatbazis2\\Hazik\\XML_fajlok\\nested.xml", XmlWriteMode.IgnoreSchema);
            dsTagok.WriteXmlSchema("E:\\AAAgyetem\\4. felev\\Adatbazis2\\Hazik\\XML_fajlok\\lab5_nested.xsd");
        }
    }
}
