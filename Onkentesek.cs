﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Lab8
{
    class Onkentesek : DAL
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Onkentesek(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "Onkentesek");
        }

        public DataSet kiolvas_ds(string sBetuk, ref string Errm)
        {
            string s = "SELECT OnkentesNev FROM Onkentesek, Onkentes, Partnerek WHERE Onkentes.OnkentesID = Onkentesek.OnkentesID and Onkentes.PartnerID = Partnerek.PartnerID and PartnerNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Onkentesek", ref da, ref Errm);
            return ds;
        }
    }
}
