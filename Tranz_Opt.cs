﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Lab8
{
    public partial class Tranz_Opt : Form
    {
        public bool Err;
        System.Data.DataSet ds;
        bool filter;
        int value = -1;
        int sulyos = -2;
        int uj_suly = -1;

        public Tranz_Opt()
        {
            InitializeComponent();
        }
        

        private void Form6_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Chocolate;
            dataGridView1.BackgroundColor = System.Drawing.Color.Beige; //hatterszin
            textBox1.Enabled = false;
            button2.Enabled = false;
            button4.Visible = false;
            button5.Visible = false;
            button6.Visible = false;
            label2.Visible = false;

            Betegseg_optimalis b = new Betegseg_optimalis(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds(ref ErrM);
            dataGridView1.DataSource = ds;  //DataGridView feltoltese
            dataGridView1.DataMember = "Betegsegek";
        }

        private void button1_Click(object sender, EventArgs e)
        {//Modositas
             filter = false;
             textBox1.Enabled = true;
             try //hibakezeles
             {
                 DataGridViewRow sor1 = dataGridView1.SelectedRows[0]; //egy sor a datagridview bol
                 int value;
                 value = Convert.ToInt32(sor1.Cells[2].Value);
                 textBox1.Text = value.ToString();
             }
             catch (Exception ex)
             {
                 String Errm = "";
                 Errm = ex.Message;
             }
             if(Globalis.var != 3)
                button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {//Mentes
            Betegseg_optimalis b = new Betegseg_optimalis(ref Err);
            try //hibakezeles
            {
                DataGridViewRow sor1 = dataGridView1.SelectedRows[0]; //egy sor a datagridview bol
                value = Convert.ToInt32(sor1.Cells[0].Value);
                sulyos = Convert.ToInt32(sor1.Cells[2].Value);
            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
            string Err2 = "";
            int suly = b.sulyos(ref Err2,value);

            if (sulyos != suly)
            {

                button6.Visible = true;
                button5.Visible = true;
                button4.Visible = true;
                label2.Visible = true;
            }
            else {
                try
                {
                    uj_suly = Convert.ToInt32(textBox1.Text);
                }
                catch (Exception ex)
                {
                    String Errm = "";
                    Errm = ex.Message;
                }

                Betegseg_optimalis b2 = new Betegseg_optimalis(ref Err);
                b2.modosit(value, uj_suly);
                string ErrM = "";
                ds = b.kiolvas_ds(ref ErrM);
                dataGridView1.DataSource = ds;  //DataGridView feltoltese
                dataGridView1.DataMember = "Betegsegek";
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {//Kilepes
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //eredeti
            Betegseg_optimalis b2 = new Betegseg_optimalis(ref Err);
            b2.modosit(value, sulyos);       
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //regi
            Betegseg_optimalis b = new Betegseg_optimalis(ref Err);
            string Err2 = "";
            int suly = b.sulyos(ref Err2, value);
            b.modosit(value, suly);
            string ErrM = "";
            ds = b.kiolvas_ds(ref ErrM);
            dataGridView1.DataSource = ds;  //DataGridView feltoltese
            dataGridView1.DataMember = "Betegsegek";

        }

        private void button6_Click(object sender, EventArgs e)
        {
            //uj
            int uj_suly = -1;
            try
            {
                uj_suly = Convert.ToInt32(textBox1.Text);
            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }

            Betegseg_optimalis b2 = new Betegseg_optimalis(ref Err);
            b2.modosit(value, uj_suly);
            string ErrM = "";
            ds = b2.kiolvas_ds(ref ErrM);
            dataGridView1.DataSource = ds;  //DataGridView feltoltese
            dataGridView1.DataMember = "Betegsegek";
        }
    }
}
