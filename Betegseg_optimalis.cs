﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;
namespace Lab8
{
    public struct OneClass7
    {
        public string betegsegnev;
        public int sulyossag;
    }
    class Betegseg_optimalis : DAL
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Betegseg_optimalis(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "Betegsegek");
        }

        public int alakit()
        {
            string error2 = "";
            SqlDataReader olvaso = myExecuteReader("SELECT PartnerID FROM Partnerek Where PartnerNev = comboBox1.Text", ref error2);
            int x = Convert.ToInt32(olvaso);
            return x;
        }
        public List<OneClass7> kiolvas()
        {
            List<OneClass7> Betegseg_optimalis = new List<OneClass7>();
            OneClass7 oc = new OneClass7();
            string error = "";
            SqlDataReader reader = myExecuteReader("SELECT BetegsegID, BetegsegNev, Sulyossag FROM Betegsegek like", ref error);
            while (reader.Read())
            {
                oc.betegsegnev = reader["BetegsegNev"].ToString();
                oc.sulyossag = Convert.ToInt32(reader["Sulyossag"]);
                Betegseg_optimalis.Add(oc);
            }
            return Betegseg_optimalis;
        }
        public DataSet kiolvas_ds(ref string Errm)
        {
            string s = "SELECT BetegsegID, BetegsegNev, Sulyossag FROM Betegsegek";
            ds = base.myFillDataSetFromString(s, "Betegsegek", ref da, ref Errm);
            return ds;
        }

        public int sulyos (ref string Err, int id)
        {
            //biztos, hogy egyedi lesz a kulcs, mert a letezo legnagyobbat keressuk meg es eggyel nagyobb lesz az uj
            //kesobb nem kell tesztelni
            sQuery = "SELECT Sulyossag from Betegsegek where BetegsegID = "+ id;
            int suly = base.myExecuteScalar(sQuery, ref Err);
            return suly;
        }

        public void modosit(int ID, int suly)
        {
            OpenConnection();
            string s = "UPDATE Betegsegek SET Sulyossag = "+ suly+" WHERE BetegsegID = " + ID;
            base.myInsertCommand(s);
            CloseConnection();
        }
    }
}
