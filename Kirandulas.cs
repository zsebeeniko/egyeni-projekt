﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;

namespace Lab8
{
    public class Kirandulas
    {
        SqlConnection conn;
        SqlCommand cmd;
        SqlDataReader dr;
        string connectionstring = "Data Source=(local);Initial Catalog=projekt;Integrated Security=SSPI";

        string ErrMess;
        ArrayList al1 = new ArrayList();
        //adatbazissal valo kapcsolat megnyitasa
        public void OpenConnection(ref string ErrMess)
        {
            try
            {
                conn = new SqlConnection(connectionstring);
                conn.Open();

                ErrMess = "ok";
            }
            catch (Exception ex)
            {
                ErrMess = ex.Message;
            }
        }
        //adatbazissal valo kapcsolat bezarasa
        public void CloseConnection(ref string ErrMess)
        {
            try
            {
                conn.Close();
                ErrMess = "ok";
            }
            catch (Exception ex)
            {
                ErrMess = ex.Message;
            }
        }
        //a ComboBox-ba toltendo adatok kinyerese az adatbazisbol
        public ArrayList Feltolt()
        {
            try
            {
                OpenConnection(ref ErrMess);
                if (ErrMess == "ok")
                {
                    cmd = new SqlCommand("SELECT KirandulasNev FROM Kirandulasok", conn);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        al1.Add(dr["KirandulasNev"].ToString());
                    }
                }
                dr.Close();
                CloseConnection(ref ErrMess);
            }
            catch (Exception ex)
            {
                ErrMess = ex.Message;
            }
            return al1;
        }
    }
}