﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Lab8
{
    public partial class Form4 : Form
    {
        public int valtozo;
        public bool Err;
        public bool First;
        public string errmess;
        public bool error;
        public string valaszt;

        public ArrayList al1;
        Kirandulas a1 = new Kirandulas();
        
        bool filter = false;

        public ArrayList al3;
        TCsapat a3 = new TCsapat();

        public ArrayList al5;
        Partner a5 = new Partner();

        public ArrayList al2;
        Betegsegek a2 = new Betegsegek();
        
        System.Data.DataSet ds, ds2;

        public Form4()
        {
            InitializeComponent();
        }
        private DataSet tamogat1;

        private void Form4_Load(object sender, EventArgs e)
        {
                          
            this.BackColor = System.Drawing.Color.Chocolate;
            dataGridView1.BackgroundColor = System.Drawing.Color.Beige;
            dataGridView2.BackgroundColor = System.Drawing.Color.Beige;
            dataGridView3.BackgroundColor = System.Drawing.Color.Beige;
            dataGridView4.BackgroundColor = System.Drawing.Color.Beige;

            First = true;
            al1 = new ArrayList();
            al1 = a1.Feltolt();

            foreach (string c in al1)
            {
                comboBox1.Items.Add(c);
            }
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            al5 = new ArrayList();
            al5 = a5.Feltolt();

            foreach (string c in al5)
            {
                comboBox5.Items.Add(c);
            }
            comboBox5.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            al3 = new ArrayList();
            al3 = a3.Feltolt();

            foreach (string c in al3)
            {
                comboBox3.Items.Add(c);
            }
            comboBox3.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            al2 = new ArrayList();
            al2 = a2.Feltolt();

            foreach (string c in al2)
            {
                comboBox2.Items.Add(c);
            }
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            string ErrM2 = "";
            Tarolt t = new Tarolt(ref Err);
            string pr3 = t.procedure3(ref ErrM2);
            textBox4.Text = pr3;
            string pr4 = t.procedure4(ref ErrM2);
            textBox2.Text = pr4;
           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!First)
            {
                dataGridView1.DataSource = null;
            }
            else
            {
                First = false;
            } //egyszerre modositja a datagridviewy a kivalasztott elem szerint, mely a comboBoxban talalhato
            Csalad b = new Csalad(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds10(comboBox1.Text, ref ErrM);
     
            dataGridView2.DataSource = ds;
            dataGridView2.DataMember = "Csaladok";

            string ErrM2 = "";
            Tarolt t = new Tarolt(ref Err);
            string pr1 = t.procedure1(comboBox1.Text, ref ErrM2);
            textBox5.Text = pr1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!First)
            {
                dataGridView3.DataSource = null;
            }
            else
            {
                First = false;
            }
            Tervezok b = new Tervezok(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds(comboBox3.Text, ref ErrM);
            dataGridView3.DataSource = ds;
            dataGridView3.DataMember = "Tervezok";

            if (!First)
            {
                dataGridView1.DataSource = null;
            }
            else
            {
                First = false;
            }

            Csapat c = new Csapat(ref Err);
            string ErrM2 = "";
            ds2 = c.kiolvas_ds3(comboBox3.Text, ref ErrM2);
            dataGridView1.DataSource = ds2;
            dataGridView1.DataMember = "TervezoCsapatok";    

        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!First)
            {
                dataGridView4.DataSource = null;
            }
            else
            {
                First = false;
            }
            Onkentesek c = new Onkentesek(ref Err);
            string ErrM = "";
            ds = c.kiolvas_ds(comboBox5.Text, ref ErrM);
            dataGridView4.DataSource = ds;
            dataGridView4.DataMember = "Onkentesek";
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {//tarolt eljaras 
            string ErrM2 = "";
            Tarolt t = new Tarolt(ref Err);
            string pr2 = t.procedure2(comboBox2.Text, ref ErrM2);
            textBox1.Text = pr2;
        }
        // tarolt eljaras meghivasa, amely egy DataSet-et terit vissza
        private void button2_Click(object sender, EventArgs e)
        {
            string ErrM2 = "";
            Tarolt t = new Tarolt(ref Err);
            if (checkBox1.Checked)
            {
                tamogat1 = t.procedure6(Convert.ToInt32(textBox2.Text), ref ErrM2);
                dataGridView1.DataSource = tamogat1;
                dataGridView1.DataMember = "tam";
            }
            else
            {
                tamogat1 = t.procedure5(Convert.ToInt32(textBox2.Text), ref ErrM2);
                dataGridView1.DataSource = tamogat1;
                dataGridView1.DataMember = "tam";
            }
        }
    }
}
