﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Lab8
{
    public partial class Form1 : Form
    {
        //valtozok deklaralasa
        public int valtozo;
        public bool Err;
        public bool First;
        public string errmess;
        public bool error;
        public string valaszt;
        public ArrayList al;
        Partner a = new Partner();
        bool filter = false; //szures

        public ArrayList al1;
        Beszallito a1 = new Beszallito();
        System.Data.DataSet ds;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Chocolate;
            dataGridView1.BackgroundColor = System.Drawing.Color.Beige; //hatterszin
            First = true;
            al = new ArrayList();
            al = a.Feltolt(); 
            //comboBox feltoltese
            foreach (string c in al)
            {
                comboBox1.Items.Add(c);
            }
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            //masik comboBox feltoltese
            al1 = new ArrayList();
            al1 = a1.Feltolt();

            foreach (string c in al1)
            {
                comboBox2.Items.Add(c);
            }
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            Csapat b = new Csapat(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds(ref ErrM);
            dataGridView1.DataSource = ds;  //DataGridView feltoltese
            dataGridView1.DataMember = "TervezoCsapatok";
            this.Kezdet();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           if (filter == false)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csapat b = new Csapat(ref Err);

                string ErrM = "";
                ds = b.kiolvas_ds(ref ErrM);
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "TervezoCsapatok";
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filter == true)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csapat b = new Csapat(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds1(comboBox1.Text, ref ErrM);
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "TervezoCsapatok";
            }
        }

        private void Uj()
        {
            but_ment.Enabled = true;
            but_megse.Enabled = true;
            but_uj.Enabled = false;
            but_modosit.Enabled = false;
            but_torol.Enabled = false;
            but_kilep.Enabled = false;
        }

        private void Kezdet()
        {
            if (Globalis.var == 1)
            {
                but_ment.Enabled = false;
                but_megse.Enabled = false;
                but_uj.Enabled = true;
                but_modosit.Enabled = true;
                but_torol.Enabled = true;
                but_kilep.Enabled = true;
            }
            else 
            {
                but_ment.Enabled = false;
                but_megse.Enabled = false;
                but_uj.Enabled = false;
                but_modosit.Enabled = false;
                but_torol.Enabled = false;
                but_kilep.Enabled = true;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filter == true)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csapat b = new Csapat(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds2(comboBox2.Text, ref ErrM);
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "TervezoCsapatok";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Uj gomb

            filter = false;
            valaszt = "uj";
            textBox1.Enabled = true;
            textBox1.Text = "";
            comboBox1.SelectedItem = null;
            comboBox2.SelectedItem = null;
            this.Uj();
            valtozo = -1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Modositas gomb
            filter = false;
            try //hibakezeles
            {
                DataGridViewRow sor1 = dataGridView1.SelectedRows[0]; //egy sor a datagridview bol
                string value1;
                string value2;
                string value3;
                                
                    //a modositashoz szukseges adatok
                    valtozo = Convert.ToInt32(sor1.Cells[0].Value);
                    value1 = sor1.Cells[1].Value.ToString();
                    value2 = sor1.Cells[3].Value.ToString();
                    value3 = sor1.Cells[2].Value.ToString();
                   
                    textBox1.Text = value1;
                    comboBox1.SelectedItem = value2;
                    comboBox2.SelectedItem = value3;

                    but_ment.Enabled = true;
                    but_megse.Enabled = true;
                    but_uj.Enabled = false;
                    but_modosit.Enabled = false;
                    but_torol.Enabled = false;
                  
                
            }
            catch(Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
        }

        private CurrencyManager CM
        {
            get
            {
                return this.dataGridView1.BindingContext[this.dataGridView1.DataSource, this.dataGridView1.DataMember] as CurrencyManager;
            }
        }


        private DataGridViewRow CurrentRow
        {
            get
            {
                if (this.CM == null) return null;
                if (this.CM.Position == -1) return null;
                return dataGridView1.SelectedRows[0];
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            //Torles gomb
            DialogResult result;
            result = MessageBox.Show("Biztos ki akarja torolni az elemet", "", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    SqlConnection conn = new SqlConnection();
                    conn.ConnectionString = "Data Source=(local);Initial Catalog=projekt;Integrated Security=SSPI";
                    conn.Open();

                    DataGridViewRow item = dataGridView1.SelectedRows[0];
                    string queryDeleteString1 = ("DELETE FROM Tervez WHERE CsapatID =" + Convert.ToInt32(item.Cells[0].Value) + "");
                    SqlCommand sqlDelete = new SqlCommand();
                    sqlDelete.CommandText = queryDeleteString1;
                    sqlDelete.Connection = conn;
                    sqlDelete.ExecuteNonQuery();

                    string queryDeleteString2 = ("DELETE FROM Tamogat WHERE CsapatID =" + Convert.ToInt32(item.Cells[0].Value) + "");
                    SqlCommand sqlDelete2 = new SqlCommand();
                    sqlDelete.CommandText = queryDeleteString1;
                    sqlDelete.Connection = conn;
                    sqlDelete.ExecuteNonQuery();

                    string queryDeleteString3 = ("DELETE FROM Csaladok WHERE CsapatID =" + Convert.ToInt32(item.Cells[0].Value) + "");
                    SqlCommand sqlDelete3 = new SqlCommand();
                    sqlDelete.CommandText = queryDeleteString1;
                    sqlDelete.Connection = conn;
                    sqlDelete.ExecuteNonQuery();

                    string queryDeleteString = ("DELETE FROM TervezoCsapatok WHERE CsapatID =" + Convert.ToInt32(item.Cells[0].Value) + "");
                    sqlDelete.CommandText = queryDeleteString;
                    sqlDelete.Connection = conn;
                    sqlDelete.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    String Errm = "";
                    Errm = ex.Message;
                }
            }
            
            Csapat c = new Csapat(ref Err);
            string ErrM1 = "";
            ds = c.kiolvas_ds(ref ErrM1);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "TervezoCsapatok";
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Mentes gomb
            try
            {
                if (valtozo == -1)
                {
                    string ss1 = "", ss2 = "";
                    if (comboBox2.SelectedItem != null)
                    {
                        ss1 = comboBox2.SelectedItem.ToString(); // Beszallitok
                    }
                    else
                    {
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    if (comboBox1.SelectedItem != null)
                    {
                        ss2 = comboBox1.SelectedItem.ToString();
                        // Partnerek
                    }
                    else {
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    string ss3 = textBox1.Text.ToString();    // CsapatNev

                    string Err = "";
                    bool Err1 = true;
                    Csapat a3 = new Csapat(ref Err1);
                      
                       if ( ss3 == "")
                        {
                            DialogResult result2;
                            result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                        }
                        else
                        {
                            a3.beszuras(ss3, ss1, ss2);
                        }

                }
                else
                {
                    string s2 = "", s1 = "";
                    if (comboBox2.SelectedItem != null)
                    {
                        s1 = comboBox2.SelectedItem.ToString();// Beszallitok
                    }
                    else
                    {
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    if (comboBox1.SelectedIndex != null)
                     s2 = comboBox1.SelectedItem.ToString();// Partnerek
                    else
                    {
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    string s3 = textBox1.Text.ToString();    //CsapatNev

                    if (s3 == "")
                    {

                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    else
                    {
                        Csapat a2 = new Csapat(ref Err);
                        a2.modosit(valtozo, s3, s1, s2);
                    }
                    Csapat b = new Csapat(ref Err);
                    string ErrM = "";
                    ds = b.kiolvas_ds(ref ErrM);

                    dataGridView1.DataSource = ds;
                    dataGridView1.DataMember = "TervezoCsapatok";
                }
            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
            dataGridView1.Refresh();
            Csapat c = new Csapat(ref Err);
            string ErrM1 = "";
            ds = c.kiolvas_ds(ref ErrM1);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "TervezoCsapatok";
            this.Refresh();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Megse gomb
            this.Kezdet();
            comboBox1.SelectedIndex = -1;
            comboBox2.SelectedIndex = -1;
            textBox1.Text = "";
            this.Refresh();
            dataGridView1.Refresh();
            Csapat b = new Csapat(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds(ref ErrM);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "TervezoCsapatok";
        }

        private void but_kilep_Click(object sender, EventArgs e)
        {//kilepes
            this.Close();
        }
    }
}
