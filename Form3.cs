﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Lab8
{
    public partial class Form3 : Form
    {
        public int valtozo;
        public bool Err;
        public bool First;
        public string errmess;
        public bool error;
        public string valaszt;
        bool filter = false;

        public ArrayList al;
        Allam a = new Allam();

        public ArrayList cl;
        Kirandulas ck = new Kirandulas();

        public ArrayList bl;
        Karosodas b = new Karosodas();

        public ArrayList dl;
        TCsapat d = new TCsapat();
        System.Data.DataSet ds, ds2;

        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Chocolate; //a form szine
            dataGridView1.BackgroundColor = System.Drawing.Color.Beige; //a datagridview-k szine
            dataGridView2.BackgroundColor = System.Drawing.Color.Beige;
            First = true;
            al = new ArrayList();
            al = a.Feltolt();
            //comboBoxok feltoltese
            foreach (string c in al)
            {
                comboBox1.Items.Add(c);
            }
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            First = true;
            bl = new ArrayList();
            bl = b.Feltolt();

            foreach (string c in bl)
            {
                comboBox2.Items.Add(c);
            }
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            First = true;
            dl = new ArrayList();
            dl = d.Feltolt();

            foreach (string c in dl)
            {
                comboBox3.Items.Add(c);
            }
            comboBox3.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast
            
            First = true;
            cl = new ArrayList();
            cl = ck.Feltolt();

            foreach (string c in cl)
            {
                comboBox4.Items.Add(c);
            }
            comboBox4.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            Csalad ec = new Csalad(ref Err);
            string ErrM = "";
            ds = ec.kiolvas_ds(ref ErrM);
            ds2 = ec.kiolvas_tag(ref ErrM);
            dataGridView2.DataSource = ds2;
            dataGridView2.DataMember = "Csaladok";
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Csaladok";
            this.Kezdet();

        }

        private void Uj()
        {
            but_ment.Enabled = true;
            but_megse.Enabled = true;
            but_uj.Enabled = false;
            but_modosit.Enabled = false;
            but_torol.Enabled = false;
            but_kilep.Enabled = false;
        }

        private void Kezdet()
        {
            if (Globalis.var == 3)
            {
                but_ment.Enabled = false;
                but_megse.Enabled = false;
                but_uj.Enabled = false;
                but_modosit.Enabled = false;
                but_torol.Enabled = false;
                but_kilep.Enabled = true;
            }
            else
            {
                but_ment.Enabled = false;
                but_megse.Enabled = false;
                but_uj.Enabled = true;
                but_modosit.Enabled = true;
                but_torol.Enabled = true;
                but_kilep.Enabled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (filter == false)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csalad b = new Csalad(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds( ref ErrM);
                ds2 = b.kiolvas_tag(ref ErrM);
                dataGridView2.DataSource = ds2;
                dataGridView2.DataMember = "Csaladok";
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Csaladok";
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (filter == false)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csalad b = new Csalad(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds( ref ErrM);
                ds2 = b.kiolvas_tag(ref ErrM);
                dataGridView2.DataSource = ds2;
                dataGridView2.DataMember = "Csaladok";
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Csaladok";
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filter == true)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csalad b = new Csalad(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds1(comboBox1.Text, ref ErrM);
                ds2 = b.kiolvas_tag(ref ErrM);
                dataGridView2.DataSource = ds2;
                dataGridView2.DataMember = "Csaladok";
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Csaladok";
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filter == true)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csalad b = new Csalad(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds2(comboBox1.Text, ref ErrM);
                ds2 = b.kiolvas_tag(ref ErrM);
                dataGridView2.DataSource = ds2;
                dataGridView2.DataMember = "Csaladok";
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Csaladok";
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filter == true)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csalad b = new Csalad(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds3(comboBox1.Text, ref ErrM);
                ds2 = b.kiolvas_tag(ref ErrM);
                dataGridView2.DataSource = ds2;
                dataGridView2.DataMember = "Csaladok";
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Csaladok";
            }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filter == true)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Csalad b = new Csalad(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds4(comboBox1.Text, ref ErrM);
                ds2 = b.kiolvas_tag(ref ErrM);
                dataGridView2.DataSource = ds2;
                dataGridView2.DataMember = "Csaladok";
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Csaladok";
            }
        }

        private void but_uj_Click(object sender, EventArgs e)
        {
            filter = false;
            valaszt = "uj";
            textBox1.Enabled = true;
            textBox1.Text = "";
            comboBox1.SelectedItem = null;
            comboBox2.SelectedItem = null;
            textBox2.Enabled = true;
            textBox2.Text = "";
            comboBox3.SelectedItem = null;
            comboBox4.SelectedItem = null;
            this.Uj();
            valtozo = -1;

        }

        private void but_modosit_Click(object sender, EventArgs e)
        {
            //Modositas gomb
            filter = false;
            try //hibakezeles
            {
                DataGridViewRow sor1 = dataGridView1.SelectedRows[0];
                string value1;
                string value2;
                string value3;
                string value4;
                string value5;
                string value6;

                // ID uj oszlop +  a modositashoz szukseges adatok
                valtozo = Convert.ToInt32(sor1.Cells[0].Value);
                value1 = sor1.Cells[1].Value.ToString();
                value2 = sor1.Cells[2].Value.ToString();
                value3 = sor1.Cells[3].Value.ToString();
                value4 = sor1.Cells[4].Value.ToString();
                value5 = sor1.Cells[6].Value.ToString();
                value6 = sor1.Cells[5].Value.ToString();
                textBox1.Text = value1;
                textBox2.Text = value2;
                comboBox1.SelectedItem = value3;
                comboBox2.SelectedItem = value4;
                comboBox3.SelectedItem = value5;
                comboBox4.SelectedItem = value6;

                but_ment.Enabled = true;
                but_megse.Enabled = true;
                but_uj.Enabled = false;
                but_modosit.Enabled = false;
                but_torol.Enabled = false;
                

            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
        }

            private CurrencyManager CM
            {
                get
                {
                    return this.dataGridView1.BindingContext[this.dataGridView1.DataSource, this.dataGridView1.DataMember] as CurrencyManager;
                }
            }


            private DataGridViewRow CurrentRow
            {
                get
                {
                    if (this.CM == null) return null;
                    if (this.CM.Position == -1) return null;
                    return dataGridView1.SelectedRows[0];
                }
            }

        private void but_torol_Click(object sender, EventArgs e)
        {
            //Torles gomb
            DialogResult result;
            result = MessageBox.Show("Biztos ki akarja torolni az elemet", "", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    SqlConnection conn = new SqlConnection();
                    conn.ConnectionString = "Data Source=(local);Initial Catalog=projekt;Integrated Security=SSPI";
                    conn.Open();

                    DataGridViewRow item = dataGridView1.SelectedRows[0];
                    //string queryDeleteString1 = ("DELETE FROM Tagok WHERE CsaladID =" + Convert.ToInt32(item.Cells[0].Value) + "");
                    SqlCommand sqlDelete = new SqlCommand();
                    //sqlDelete.CommandText = queryDeleteString1;
                    //sqlDelete.Connection = conn;
                    //sqlDelete.ExecuteNonQuery();

                    string queryDeleteString = ("DELETE FROM Csaladok WHERE CsaladID =" + Convert.ToInt32(item.Cells[0].Value) + "");
                    sqlDelete.CommandText = queryDeleteString;
                    sqlDelete.Connection = conn;
                    sqlDelete.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    String Errm = "";
                    Errm = ex.Message;
                }
            }
            
            Csalad c = new Csalad(ref Err);
            string ErrM1 = "";
            ds = c.kiolvas_ds(ref ErrM1);
            ds2 = c.kiolvas_tag(ref ErrM1);
            dataGridView2.DataSource = ds2;
            dataGridView2.DataMember = "Csaladok";
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Csaladok";
            
        }

        private void but_ment_Click(object sender, EventArgs e)
        {
            //Mentes gomb
            try
            {
                if (valtozo == -1) // uj vagy modositas letesztelese ...
                {
                    int szamol = 0;
                    string ss3 = "", ss4 = "", ss5 = "", ss6 = "", ss2 = "";
                    if (comboBox1.SelectedItem != null)
                    {
                        ss3 = comboBox1.SelectedItem.ToString(); // Allamok
                    }
                    else
                    {
                        szamol = szamol + 1;
                    }
                    if (comboBox2.SelectedItem != null)
                    {
                        ss4 = comboBox2.SelectedItem.ToString(); // Karosodasok
                    }
                    else
                    {
                        szamol = szamol + 1;
                    }
                    string ss1 = textBox1.Text.ToString();    // CsaladNev
                    if (comboBox4.SelectedItem != null)
                    {
                        ss5 = comboBox4.SelectedItem.ToString(); // Kirandulasok
                    }
                    else
                    {
                        szamol = szamol + 1;
                    }
                    if (comboBox3.SelectedItem != null)
                    {
                        ss6 = comboBox3.SelectedItem.ToString(); // Csapat
                    }
                    else
                    {
                        szamol = szamol + 1;
                    }
                   
                    ss2 = textBox2.Text.ToString();    //TagokSzama
                 
                    if ((ss2 == "") || (ss1 == ""))
                        szamol = szamol + 1;
                    if (szamol == 0)
                    {
                        string Err = "";
                        bool Err1 = true;
                        Csalad a3 = new Csalad(ref Err1);


                        a3.beszuras(ss1, ss2, ss3, ss4, ss5, ss6);
                    }
                    else
                    {
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                }
                else
                {
                    int szamol2 = 0;
                    string s3 = "", s4 = "", s5 = "", s6 = "";
                    if (comboBox1.SelectedItem != null)
                        s3 = comboBox1.SelectedItem.ToString(); // Allamok
                    else
                        szamol2 = szamol2 + 1;
                    if (comboBox2.SelectedItem != null)
                        s4 = comboBox2.SelectedItem.ToString(); // Karosodasok
                    else 
                        szamol2 = szamol2 + 1;
                    string s1 = textBox1.Text.ToString();    // CsaladNev
                    if (comboBox3.SelectedItem != null)
                        s5 = comboBox3.SelectedItem.ToString(); // Kirandulasok
                    else
                        szamol2 = szamol2 + 1;
                    if (comboBox4.SelectedItem != null)
                        s6 = comboBox4.SelectedItem.ToString(); // Csapat
                    else
                        szamol2 = szamol2 + 1;
                    string s2 = textBox2.Text.ToString();    //TagokSzama

                    if ((s2 == "") || (s1 == ""))
                        szamol2 = szamol2 + 1;

                    if (szamol2 == 0)
                    {
                        Csalad a2 = new Csalad(ref Err);
                        a2.modosit(valtozo, s1, s2, s3, s4, s5, s6);
                    }
                    else
                    {
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    Csalad b = new Csalad(ref Err);
                    string ErrM = "";
                    ds = b.kiolvas_ds(ref ErrM);
                    ds2 = b.kiolvas_tag(ref ErrM);
                    dataGridView2.DataSource = ds2;
                    dataGridView2.DataMember = "Csaladok";
                    dataGridView1.DataSource = ds;
                    dataGridView1.DataMember = "Csaladok";
                }
            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
            dataGridView1.Refresh();
            Csalad c = new Csalad(ref Err);
            string ErrM1 = "";
            ds = c.kiolvas_ds(ref ErrM1);
            ds2 = c.kiolvas_tag(ref ErrM1);
            dataGridView2.DataSource = ds2;
            dataGridView2.DataMember = "Csaladok";
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Csaladok";
            this.Refresh();
        }

        private void but_megse_Click(object sender, EventArgs e)
        {
            //Megse gomb
            this.Kezdet();
            comboBox1.SelectedIndex = -1;
            comboBox2.SelectedIndex = -1;
            textBox1.Text = "";
            comboBox3.SelectedIndex = -1;
            comboBox4.SelectedIndex = -1;
            textBox2.Text = "";
            this.Refresh();
            dataGridView1.Refresh();
            Csalad b = new Csalad(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds(ref ErrM);
            ds2 = b.kiolvas_tag(ref ErrM);
            dataGridView2.DataSource = ds2;
            dataGridView2.DataMember = "Csaladok";
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Csaladok";
        }

        private void but_kilep_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
