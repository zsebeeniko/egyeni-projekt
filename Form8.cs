﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8
{
    public partial class Form8 : Form
    {
        public Form8()
        {
            InitializeComponent();
        }

        private void Form8_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Chocolate; //a form szine
 //           dataGridView1.BackgroundColor = System.Drawing.Color.Beige; //a datagridview-k szine
            if (Globalis.var == 1)
            {
                //radioButton1.Enabled = true;
                radioButton2.Enabled = true;
                radioButton3.Enabled = true;
                //radioButton1.Checked = false;
                radioButton2.Checked = false;
                radioButton3.Checked = false;
                button1.Enabled = true;
                button2.Enabled = true;

            }
            else 
            {
                button1.Enabled = false;
                //button2.Enabled = false;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            // Regisztracio
            int szam = 0; // hany adat nincs kitoltve
            string teljes_nev = "", felh_nev = "", jelszo = "", tipus = "", kriptalt = "";

            string Err = "";
            bool Err1 = true;
            Regisztracio r = new Regisztracio(ref Err1);
            
            teljes_nev = textBox1.Text.ToString();    // Teljes Nev
            felh_nev = textBox2.Text.ToString();
            jelszo = textBox3.Text.ToString();
            kriptalt = r.GetMd5Hash(jelszo);

            //if (radioButton1.Checked == true)
            //{
              //  tipus = radioButton1.Text.ToString();
            //}
            if (radioButton2.Checked == true)
            {
                tipus = radioButton2.Text.ToString();
            }
            else if (radioButton3.Checked = true)
            {
                tipus = radioButton3.Text.ToString();
            }

            if (teljes_nev == "")
                szam++;
            if (felh_nev == "")
                szam++;
            if (kriptalt == "")
                szam++;
            if (tipus == "")
                szam++;

            if (r.letezike(felh_nev) == 0)
            {
                if (szam == 0)
                {
                    r.regisztralas(teljes_nev, felh_nev, kriptalt, tipus);
                    DialogResult result2;
                    result2 = MessageBox.Show("Gratulalok ! Sikeres regisztracio !");
                    Bejelentkezes login = new Bejelentkezes();
                    login.Show();
                    this.Close();
                    
                }
                else
                {
                    DialogResult result2;
                    result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                }
            }
            else
            {
                DialogResult result2;
                result2 = MessageBox.Show("Ez a felhasznalonev mar letezik ! Probaljon ujat !");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Kilepes
            this.Close();
        }
    }
}
