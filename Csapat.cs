﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Lab8
{
    public struct OneClass5
    {
        public string csapatnev;
        public int beszallito;
        public int partner;
    }
    public class Csapat : DAL
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Csapat(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "TervezoCsapatok");
        }

        public int alakit()
        {
            string error2 = "";
            SqlDataReader olvaso = myExecuteReader("SELECT PartnerID FROM Partnerek Where PartnerNev = comboBox1.Text", ref error2);
            int x = Convert.ToInt32(olvaso);
            return x;
        }
        public List<OneClass5> kiolvas()
        {
            List<OneClass5> Csapatok = new List<OneClass5>();
            OneClass5 oc = new OneClass5();
            string error = "";
            SqlDataReader reader = myExecuteReader("SELECT CsapatNev, Beszallitok.BeszallitoID, Partnerek.PartnerID FROM TervezoCsapatok, Beszallitok, Partnerek WHERE TervezoCsapatok.BeszallitoID=Beszallitok.BeszallitoID and TervezoCsapatok.PartnerID = Partnerek.PartnerID and BeszallitoNev like", ref error);
            while (reader.Read())
            {
                oc.csapatnev = reader["CsapatNev"].ToString();
                oc.beszallito= Convert.ToInt32(reader["BeszallitoID"]);
                oc.partner = Convert.ToInt32(reader["PartnerID"]);
                Csapatok.Add(oc);
            }
            return Csapatok;
        }
        public DataSet kiolvas_ds(ref string Errm)
        {
            string s = "SELECT TervezoCsapatok.CsapatID, CsapatNev, Beszallitok.BeszallitoNev, Partnerek.PartnerNev FROM TervezoCsapatok, Partnerek, Beszallitok WHERE TervezoCsapatok.PartnerID=Partnerek.PartnerID and TervezoCsapatok.BeszallitoID = Beszallitok.BeszallitoID";
            ds = base.myFillDataSetFromString(s, "TervezoCsapatok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds1(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsapatNev, Beszallitok.BeszallitoID, Partnerek.PartnerID FROM TervezoCsapatok, Partnerek, Beszallitok WHERE TervezoCsapatok.PartnerID=Partnerek.PartnerID and TervezoCsapatok.BeszallitoID = Beszallitok.BeszallitoID and BeszallitoNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "TervezoCsapatok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds2(string sBetuk, ref string Errm)
        {
            string s ="SELECT CsapatNev, Beszallitok.BeszallitoID, Partnerek.PartnerID FROM TervezoCsapatok, Partnerek, Beszallitok WHERE TervezoCsapatok.PartnerID=Partnerek.PartnerID and TervezoCsapatok.BeszallitoID = Beszallitok.BeszallitoID and PartnerNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "TervezoCsapatok", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds3(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsapatID, Beszallitok.BeszallitoID, Beszallitok.BeszallitoNev, Partnerek.PartnerID, Partnerek.PartnerNev FROM TervezoCsapatok, Partnerek, Beszallitok WHERE TervezoCsapatok.PartnerID=Partnerek.PartnerID and TervezoCsapatok.BeszallitoID = Beszallitok.BeszallitoID and CsapatNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "TervezoCsapatok", ref da, ref Errm);
            return ds;
        }

        public void beszuras(string csapatnev, string beszallito, string partner)
        {
            OpenConnection();
            //beszurunk egy uj sort az TervezoCsapatok tablaba
            string Err = "";
            sQuery = "SELECT BeszallitoID FROM Beszallitok WHERE BeszallitoNev = '" + beszallito + "'";
            int beszallitoid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT PartnerID FROM Partnerek WHERE PartnerNev = '" + partner + "'";
            int partnerid = base.myExecuteScalar(sQuery, ref Err);

            string s = "INSERT INTO TervezoCsapatok (CsapatNev, BeszallitoID, PartnerID) VALUES ('" + csapatnev + "'," + beszallitoid + "," + partnerid+")";

            base.myInsertCommand(s);
            CloseConnection();
        }

        public int MaxID(ref string Err)
        {
            //biztos, hogy egyedi lesz a kulcs, mert a letezo legnagyobbat keressuk meg es eggyel nagyobb lesz az uj
            //kesobb nem kell tesztelni
            sQuery = "SELECT MAX(SzolgID) FROM Szolgaltatasok";
            int iKod = base.myExecuteScalar(sQuery, ref Err);
            return iKod;
        }

        public void modosit(int ID, String csapatnev, String beszallito, String partner)
        {
            OpenConnection();
            string Err = "";
            sQuery = "SELECT BeszallitoID FROM Beszallitok WHERE BeszallitoNev = '" + beszallito + "'";
            int beszallitoid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT PartnerID FROM Partnerek WHERE PartnerNev = '" + partner + "'";
            int partnerid = base.myExecuteScalar(sQuery, ref Err);

            string s = "UPDATE TervezoCsapatok SET CsapatNev = '" + csapatnev + "', BeszallitoID = " + beszallitoid + ", PartnerID = " + partnerid + "  WHERE CsapatID = " + ID;

            base.myInsertCommand(s);
            CloseConnection();
        }

    }
}