﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Lab8
{
    public struct OneClass6
    {
        public int csapatid;
        public int tamogatoid;
        public string csapatnev;
        public string tamogatonev;
        public string datum;
        public int osszeg;
    }
    public class Tamogatok2 : DAL
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Tamogatok2(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "Tamogat");
        }

        public int alakit()
        {
            string error2 = "";
            SqlDataReader olvaso = myExecuteReader("SELECT PartnerID FROM Partnerek Where PartnerNev = comboBox1.Text", ref error2);
            int x = Convert.ToInt32(olvaso);
            return x;
        }
        public List<OneClass6> kiolvas()
        {
            List<OneClass6> Tamogatok = new List<OneClass6>();
            OneClass6 oc = new OneClass6();
            string error = "";
            SqlDataReader reader = myExecuteReader("SELECT Tamogat.TamogatoID, Tamogat.CsapatID, TervezoCsapatok.CsapatNev, Tamogatok.TamogatoNev, Datum, Osszeg FROM Tamogat, TervezoCsapatok, Tamogatok WHERE Tamogatok.TamogatoID = Tamogat.TamogatID and TervezoCsapatok.CsapatID = Tamogat.csapatID and CsapatNev like", ref error);
            while (reader.Read())
            {
                oc.tamogatoid = Convert.ToInt32(reader["TamogatoID"]);
                oc.csapatid = Convert.ToInt32(reader["CsapatID"]);
                oc.csapatnev = reader["CsapatNev"].ToString();
                oc.datum = reader["Datum"].ToString();
                oc.osszeg = Convert.ToInt32(reader["Osszeg"]);
                oc.tamogatonev = reader["TamogatoNev"].ToString();
                Tamogatok.Add(oc);
            }
            return Tamogatok;
        }
        public DataSet kiolvas_ds(ref string Errm)
        {
            string s = "SELECT Tamogat.CsapatID, Tamogat.TamogatoID, TervezoCsapatok.CsapatNev, Tamogatok.TamogatoNev, Datum, Osszeg FROM TervezoCsapatok, Tamogatok, Tamogat WHERE TervezoCsapatok.CsapatID=Tamogat.CsapatID and Tamogat.TamogatoID = Tamogatok.TamogatoID";
            ds = base.myFillDataSetFromString(s, "Tamogat", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds1(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsapatNev, Beszallitok.BeszallitoID, Partnerek.PartnerID FROM TervezoCsapatok, Partnerek, Beszallitok WHERE TervezoCsapatok.PartnerID=Partnerek.PartnerID and TervezoCsapatok.BeszallitoID = Beszallitok.BeszallitoID and BeszallitoNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Tamogat", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds2(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsapatNev, Beszallitok.BeszallitoID, Partnerek.PartnerID FROM TervezoCsapatok, Partnerek, Beszallitok WHERE TervezoCsapatok.PartnerID=Partnerek.PartnerID and TervezoCsapatok.BeszallitoID = Beszallitok.BeszallitoID and PartnerNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Tamogat", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_ds3(string sBetuk, ref string Errm)
        {
            string s = "SELECT CsapatID, Beszallitok.BeszallitoID, Beszallitok.BeszallitoNev, Partnerek.PartnerID, Partnerek.PartnerNev FROM TervezoCsapatok, Partnerek, Beszallitok WHERE TervezoCsapatok.PartnerID=Partnerek.PartnerID and TervezoCsapatok.BeszallitoID = Beszallitok.BeszallitoID and CsapatNev LIKE '" + sBetuk + "%'";
            ds = base.myFillDataSetFromString(s, "Tamogat", ref da, ref Errm);
            return ds;
        }

        public DataSet kiolvas_tamogat(ref string Errm, string datum1, string datum2, string csapatnev, string tamogato)
        {
            string s = "SELECT Osszeg, Datum FROM Tamogat, Tamogatok, TervezoCsapatok WHERE Tamogat.TamogatoID = Tamogatok.TamogatoID and Tamogat.CsapatID = TervezoCsapatok.CsapatID and TervezoCsapatok.CsapatNev = '"+csapatnev+"' and Tamogatok.TamogatoNev = '"+tamogato+"' and Tamogat.Datum < '"+ datum2 + "' and Tamogat.Datum > '" + datum1 +"'";
            ds = base.myFillDataSetFromString(s, "Tamogat", ref da, ref Errm);
            return ds;
        }

        public void beszuras(string csapatnev, string tamogatonev, string datum, int osszeg)
        {
            OpenConnection();
            //beszurunk egy uj sort az Tamogat tablaba
            string Err = "";
            sQuery = "SELECT CsapatID FROM TervezoCsapatok WHERE CsapatNev = '" + csapatnev + "'";
            int csapatid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT TamogatoID FROM Tamogatok WHERE TamogatoNev = '" + tamogatonev + "'";
            int tamogatoid = base.myExecuteScalar(sQuery, ref Err);

            string s = "INSERT INTO Tamogat (TamogatoID, CsapatID, Osszeg, Datum) VALUES ('" + tamogatoid + "'," + csapatid + "," + osszeg + ",'" + datum +"')";
            

            base.myInsertCommand(s);
            CloseConnection();
        }

        public int Osszeg_szamla(ref string Err, string datum1, string datum2, string csapatnev, string tamogato)
        {
            sQuery = "SELECT sum(Osszeg) FROM Tamogat, Tamogatok, TervezoCsapatok WHERE Tamogat.TamogatoID = Tamogatok.TamogatoID and Tamogat.CsapatID = TervezoCsapatok.CsapatID and TervezoCsapatok.CsapatNev = '"+csapatnev+"' and Tamogatok.TamogatoNev = '"+tamogato+"' and Tamogat.Datum < '"+ datum2 + "' and Tamogat.Datum > '" + datum1 +"'";
            int ossz = base.myExecuteScalar(sQuery, ref Err);
            return ossz;
        }

        public int tamogatoid(ref string Err, string tamogatonev)
        {
            //biztos, hogy egyedi lesz a kulcs, mert a letezo legnagyobbat keressuk meg es eggyel nagyobb lesz az uj
            //kesobb nem kell tesztelni
            sQuery = "SELECT TamogatoID from Tamogatok where TamogatoNev = '" + tamogatonev + "'";
            int tamogatoid = base.myExecuteScalar(sQuery, ref Err);
            return tamogatoid;
        }

        public int szamlaid(ref string Err, string datum)
        {
            //biztos, hogy egyedi lesz a kulcs, mert a letezo legnagyobbat keressuk meg es eggyel nagyobb lesz az uj
            //kesobb nem kell tesztelni
            sQuery = "SELECT SzamlaID from Szamlak where Datum_szamla = '" + datum + "'";
            int szamlaid = base.myExecuteScalar(sQuery, ref Err);
            return szamlaid;
        }

        public int csapatid(ref string Err, string csapatnev)
        {
            //biztos, hogy egyedi lesz a kulcs, mert a letezo legnagyobbat keressuk meg es eggyel nagyobb lesz az uj
            //kesobb nem kell tesztelni
            sQuery = "SELECT CsapatID from TervezoCsapatok where CsapatNev = '" + csapatnev + "'";
            int csapatid = base.myExecuteScalar(sQuery, ref Err);
            return csapatid;
        }

        public int MaxID(ref string Err)
        {
            //biztos, hogy egyedi lesz a kulcs, mert a letezo legnagyobbat keressuk meg es eggyel nagyobb lesz az uj
            //kesobb nem kell tesztelni
            sQuery = "SELECT MAX(SzolgID) FROM Szolgaltatasok";
            int iKod = base.myExecuteScalar(sQuery, ref Err);
            return iKod;
        }

        public void modosit( int csapat, int tamogatid, String csapatnev, String tamogatonev, String datum, int osszeg)
        {
            OpenConnection();
            string Err = "";
            sQuery = "SELECT CsapatID FROM TervezoCsapatok WHERE CsapatNev = '" + csapatnev + "'";
            int csapatid = base.myExecuteScalar(sQuery, ref Err);

            sQuery = "SELECT TamogatoID FROM Tamogatok WHERE TamogatoNev = '" + tamogatonev + "'";
            int tamogatoid = base.myExecuteScalar(sQuery, ref Err);

            string s = "UPDATE Tamogat SET CsapatID = " + csapatid + ", TamogatoID = " + tamogatoid + ", Osszeg = " + osszeg + ", Datum = '"+ datum+"' WHERE CsapatID = " + csapat +" and TamogatoID = "+tamogatid+"";
            
            base.myInsertCommand(s);
            CloseConnection();
        }

    }
}