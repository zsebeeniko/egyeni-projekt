﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Lab8
{
    /// <summary>
    /// Summary description for DAL.
    /// </summary>
    public abstract class DAL
    {
        protected static bool kapcsolodva;
        protected static bool ConnectionCreated;
       // protected static SqlConnection myAccessConn;

        //ebben a projektben egy SQL szerveren futo adatbazishoz akarunk csatlakozni
        //SQL adatbazishoz SqlConnection objektumokkal kapcsolodhatunk
        protected static SqlConnection mySqlConn;

        //kapcsolati karakterlanc: szerver neve, adatbazis neve(az 5-6.laborfeladathoz tartozo adatbazis) , kapcsolodas modja

        protected string strSqlConn = "Data Source=(local);Initial Catalog=projekt;Integrated Security=SSPI";
        //protected string strSqlConn = "Data Source=(local);Initial Catalog=Berlesek;uid=userid;pwd=1234";//Integrated Security=SSPI";

        //parancsobjektum-ezek segitsegevel tudunk SQL utasitasokat vegrehajtani
        //parancsobjektumok a kapcsolati objektumok segitsegevel tartanak kapcsolatot 
        //az adatbazissal
        protected System.Data.SqlClient.SqlCommand myComm;
        //a kovetkezo ket objektum segitsegevel az adatbazisbol adatokat olvashatunk be
        //illetve tarolhatjuk ezeket
        protected System.Data.SqlClient.SqlDataReader myDataReader;
        protected System.Data.DataSet myDataSet;

        //igaz, ha a kapcsolat letrejott-a szarmaztatott osztaly konstruktoraba kell
        protected bool IsConnectCreated()
        {
            return ConnectionCreated;
        }

        protected bool IsConnected()
        {
            return kapcsolodva;
        }

        protected System.Data.SqlClient.SqlConnection theConnection()
        { return mySqlConn; }

        //kapcsolatot teremt az adatbazissal, ha ez meg nem tortent meg eddig 
        protected bool MakeConnection()
        {
            // Create the Connection if is was not already created.
            if (ConnectionCreated != true)
            {
                try
                {
                    //uj OleDbConnection objektum letrehozasa
                    //ezzel a konstruktorral letrehoztuk a kapcsolati objektumot es beallitottuk a ConnectionString
                    //tulajdonsagat is
                    //ez csak akkor allithato be, ha a kapcsolati objektum zarva van
                    mySqlConn = new SqlConnection(strSqlConn);
                    //kapcsolodunk az adatbazishoz
                    mySqlConn.Open();
                    ConnectionCreated = true; //logikai valtozo-igaz, ha a kapcsolat letrejott
                    //be is zarjuk a kapcsolatot-igyekeznunk kell minel kevesebb ideig nyitva hagyni a kapcsolatot,
                    //mert igy sporolunk az adatbazis eroforrasaival
                    mySqlConn.Close();
                    kapcsolodva = false;
                    //igazat terithetunk vissza, hisz a kapcsolat gond nelkul letrejott
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else { return true; }
        }

        //vegrehajtja az elso parameterben megadott stringben levo select utasitast
        //egy eredmenyhalmaz jon letre ezaltal, melyet egy DataReader objektumban tarolunk el
        //es teritunk vissza
        protected SqlDataReader myExecuteReader(string sQuery, ref string ErrMess)
        {
            try
            {
                this.OpenConnection();//megnyitja az adatbazis-kapcsolatot, ha meg nincs megnyitva) 

                //uj sqlcommand objektumot igy is letrehozhatunk:
                //sQuery-tartalmazza az SQL utasitast, melybol adatot kerunk le
                //mySqlConn-SqlConnection objektum 
                myComm = new SqlCommand(sQuery, mySqlConn);
                //myComm OleDbCommand CommandText tulajdonsagat atkuldi a myAccesConn
                //connectionnek es letrehoz egy DataReadert

                //ExectuerReader tagfgv-olyan select utasitasok vegrehajtasara szolgal, melyek
                //eredmenyhalmazt adnak vissza-az eredmenyhalmaz egy DataReader objektumban ter vissza
                myDataReader = myComm.ExecuteReader();
                ErrMess = "OK";

                return myDataReader;

            }
            catch (SqlException e)
            {
                ErrMess = e.Message;
                this.CloseConnection();
                return null;
            }
        }
        /// <summary>
        /// Open the Connection when the state is not already open.
        /// </summary>
        protected bool OpenConnection()
        {
            // Open the Connection when the state is not already open.
            if (kapcsolodva != true)
            {
                try
                {
                    mySqlConn.Open();
                    kapcsolodva = true;
                    return true;
                }
                catch
                {
                    return false;
                }

            }
            else { return true; }
        }

        //bezarjuk a kapcsolatot, ha meg nyitva van
        internal void CloseConnection()
        {
            // Close the Connection when the connection is opened.
            if (kapcsolodva == true)
            {
                mySqlConn.Close();
                kapcsolodva = false;
            }
        }

        protected int myExecuteScalar(string myQuery, ref string ErrMess)
        {
            int iScalar = 0;
            this.OpenConnection();
            myComm = new SqlCommand(myQuery, mySqlConn);
            try
            {
                iScalar = Convert.ToInt32(myComm.ExecuteScalar());
            }
            catch (Exception e)
            {
                ErrMess = e.Message;

            }
            this.CloseConnection();
            return iScalar;

        }
        protected DataSet myFillDataSetFromString(String sSQL, String sTableName, ref SqlDataAdapter da, ref string Errm)
        {
            //da = new OleDbDataAdapter();
            try
            {
                //		da.TableMappings.Add("Table", sTableName);
                OpenConnection();
                myComm = new SqlCommand(sSQL, mySqlConn);
                myComm.CommandType = CommandType.Text;
                da.SelectCommand = myComm;
                DataSet ds = new DataSet(sTableName);
                da.Fill(ds);
                CloseConnection();
                Errm = "OK";
                return ds;
            }
            catch (Exception ex)
            {
                Errm = ex.Message;
                return null;
            }
        }
        protected string myFillExistingDataSet(String sSQL, String sTableName, ref SqlDataAdapter da, ref DataSet ds)
        {
            //da = new OleDbDataAdapter();
            String Errm;
            try
            {
                da.TableMappings.Add("Table", sTableName);
                OpenConnection();
                myComm = new SqlCommand(sSQL, mySqlConn);
                myComm.CommandType = CommandType.Text;
                da.SelectCommand = myComm;
                //DataSet ds= new DataSet(sTableName);
                da.Fill(ds);
                CloseConnection();
                Errm = "OK";
                return Errm;
            }
            catch (Exception ex)
            {
                Errm = ex.Message;
                return Errm;
            }
        }

        protected void myInsertCommand(string sInsert)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                OpenConnection();
                da.InsertCommand = new SqlCommand(sInsert, mySqlConn);
                da.InsertCommand.ExecuteNonQuery();
                CloseConnection();
                //Errm = "OK";
            }
            catch (Exception ex)
            {
                //Errm = ex.ToString();
            }
            CloseConnection();
        }
        protected void MyDeleteComm(string sDelete)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                OpenConnection();
                da.DeleteCommand = new SqlCommand(sDelete, mySqlConn);
                da.DeleteCommand.ExecuteNonQuery();
                //Err = "OK";
            }
            catch (Exception ex)
            {
                //Err = ex.ToString();
            }
            CloseConnection();
        }

        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        ////////////// ADATBAZIS 2. /////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
        /////////////////////////////////////////////
    }
}