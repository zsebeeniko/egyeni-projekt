﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb; 
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Data;
using System.Collections;
using System.Windows.Forms;

namespace Lab8
{
    class XML_tablak : DAL
    {
        private SqlDataAdapter daCsalad = new SqlDataAdapter();
        private SqlDataAdapter daTagok = new SqlDataAdapter();
        private SqlDataAdapter daCsapat = new SqlDataAdapter();
        private SqlDataAdapter daBeszallit = new SqlDataAdapter();
        private SqlDataAdapter daTamogat = new SqlDataAdapter();
        private SqlDataAdapter daTamogatok = new SqlDataAdapter();
        private System.Data.DataSet dsBeszallit;
        private System.Data.DataRelation srTagok;
        
        public XML_tablak(ref bool Err)
		{
			if (!base.IsConnectCreated())
			{
				Err = !base.MakeConnection();
			}
			daBeszallit = new SqlDataAdapter();
			daCsapat = new SqlDataAdapter();
            daCsalad = new SqlDataAdapter();
            daTagok = new SqlDataAdapter();
            daTamogat = new SqlDataAdapter();
            daTamogatok = new SqlDataAdapter();
		}

        public System.Data.DataSet XML_general(ref string ErrM)
        {
            daBeszallit.TableMappings.Add("Table", "Beszallitok");

            String sSQL = "SELECT * FROM Beszallitok";
            dsBeszallit = base.myFillDataSetFromString(sSQL, "Beszallitok", ref daBeszallit, ref ErrM);

            //String sSQL = "SELECT * FROM Csaladok";
            //dsTagok = base.myFillDataSetFromString(sSQL, "Csaladok", ref daCsapat, ref ErrM);

            if (ErrM == "OK")
            {
                sSQL = "SELECT * FROM TervezoCsapatok";
                ErrM = base.myFillExistingDataSet(sSQL, "TervezoCsapatok", ref daCsapat, ref dsBeszallit);

                sSQL = "SELECT * FROM Tamogat";
                ErrM = base.myFillExistingDataSet(sSQL, "Tamogat", ref daTamogat, ref dsBeszallit);

                sSQL = "SELECT * FROM Tamogatok";
                ErrM = base.myFillExistingDataSet(sSQL, "Tamogatok", ref daTamogatok, ref dsBeszallit);

                sSQL = "SELECT * FROM Csaladok";
                ErrM = base.myFillExistingDataSet(sSQL, "Csaladok", ref daCsalad, ref dsBeszallit);

                sSQL = "SELECT * FROM Tagok";
                ErrM = base.myFillExistingDataSet(sSQL, "Tagok", ref daTagok, ref dsBeszallit);
            }
            if (ErrM == "OK")
            {
                DataColumn parentCol3;
                DataColumn childCol3;
                // Code to get the DataSet not shown here.
                parentCol3 = dsBeszallit.Tables["Beszallitok"].Columns["BeszallitoID"];
                childCol3 = dsBeszallit.Tables["TervezoCsapatok"].Columns["BeszallitoID"];
                // Create DataRelation.
                DataRelation relbeszallit;
                relbeszallit = new DataRelation("Beszallit", parentCol3, childCol3);
                relbeszallit.Nested = true;
                // Add the relation to the DataSet.
                dsBeszallit.Relations.Add(relbeszallit);
                                

                 // Get the DataColumn objects from two DataTable objects in a DataSet.
                DataColumn parentCol2;
                DataColumn childCol2;
                // Code to get the DataSet not shown here.
                parentCol2 = dsBeszallit.Tables["TervezoCsapatok"].Columns["CsapatID"];
                childCol2 = dsBeszallit.Tables["Csaladok"].Columns["CsapatID"];
                // Create DataRelation.
                DataRelation reltervez;
                reltervez = new DataRelation("Tervez", parentCol2, childCol2);
                reltervez.Nested = true;
                // Add the relation to the DataSet.
                dsBeszallit.Relations.Add(reltervez);

                DataColumn parentCol6;
                DataColumn childCol6;
                // Code to get the DataSet not shown here.
                parentCol6 = dsBeszallit.Tables["Tamogatok"].Columns["TamogatoID"];
                childCol6 = dsBeszallit.Tables["Tamogat"].Columns["TamogatoID"];
                // Create DataRelation.
                DataRelation reltamogatok;
                reltamogatok = new DataRelation("Tamogatok", parentCol6, childCol6);
                reltamogatok.Nested = true;
                // Add the relation to the DataSet.
                dsBeszallit.Relations.Add(reltamogatok);

                // Get the DataColumn objects from two DataTable objects in a DataSet.
                DataColumn parentCol;
                DataColumn childCol;
                // Code to get the DataSet not shown here.
                parentCol = dsBeszallit.Tables["Csaladok"].Columns["CsaladID"];
                childCol = dsBeszallit.Tables["Tagok"].Columns["CsaladID"];
                // Create DataRelation.
                DataRelation reltagja;
                reltagja = new DataRelation("Tagja", parentCol, childCol);
                reltagja.Nested = true;
                // Add the relation to the DataSet.
                dsBeszallit.Relations.Add(reltagja);

                return dsBeszallit;
            }
            else
                return null;
        }
    }
}
