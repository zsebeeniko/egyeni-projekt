﻿namespace Lab8
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menu1 = new System.Windows.Forms.MenuStrip();
            this.tervezoCsapatokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.csaladokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.szuresekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tamogatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.konkurenciavezerlesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pesszimistaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regisztracioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.szamlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.menu1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu1
            // 
            this.menu1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tervezoCsapatokToolStripMenuItem,
            this.csaladokToolStripMenuItem,
            this.szuresekToolStripMenuItem,
            this.tamogatToolStripMenuItem,
            this.konkurenciavezerlesToolStripMenuItem,
            this.pesszimistaToolStripMenuItem,
            this.regisztracioToolStripMenuItem,
            this.szamlaToolStripMenuItem});
            this.menu1.Location = new System.Drawing.Point(0, 0);
            this.menu1.Name = "menu1";
            this.menu1.Size = new System.Drawing.Size(772, 24);
            this.menu1.TabIndex = 0;
            this.menu1.Text = "TervezoCsapatok";
            this.menu1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menu1_ItemClicked);
            // 
            // tervezoCsapatokToolStripMenuItem
            // 
            this.tervezoCsapatokToolStripMenuItem.Name = "tervezoCsapatokToolStripMenuItem";
            this.tervezoCsapatokToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.tervezoCsapatokToolStripMenuItem.Text = "TervezoCsapatok";
            this.tervezoCsapatokToolStripMenuItem.Click += new System.EventHandler(this.tervezoCsapatokToolStripMenuItem_Click);
            // 
            // csaladokToolStripMenuItem
            // 
            this.csaladokToolStripMenuItem.Name = "csaladokToolStripMenuItem";
            this.csaladokToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.csaladokToolStripMenuItem.Text = "Csaladok";
            this.csaladokToolStripMenuItem.Click += new System.EventHandler(this.csaladokToolStripMenuItem_Click);
            // 
            // szuresekToolStripMenuItem
            // 
            this.szuresekToolStripMenuItem.Name = "szuresekToolStripMenuItem";
            this.szuresekToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.szuresekToolStripMenuItem.Text = "Szuresek";
            this.szuresekToolStripMenuItem.Click += new System.EventHandler(this.szuresekToolStripMenuItem_Click);
            // 
            // tamogatToolStripMenuItem
            // 
            this.tamogatToolStripMenuItem.Name = "tamogatToolStripMenuItem";
            this.tamogatToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.tamogatToolStripMenuItem.Text = "Tamogat";
            this.tamogatToolStripMenuItem.Click += new System.EventHandler(this.tamogatToolStripMenuItem_Click);
            // 
            // konkurenciavezerlesToolStripMenuItem
            // 
            this.konkurenciavezerlesToolStripMenuItem.Name = "konkurenciavezerlesToolStripMenuItem";
            this.konkurenciavezerlesToolStripMenuItem.Size = new System.Drawing.Size(126, 20);
            this.konkurenciavezerlesToolStripMenuItem.Text = "Konkurenciavezerles";
            this.konkurenciavezerlesToolStripMenuItem.Click += new System.EventHandler(this.konkurenciavezerlesToolStripMenuItem_Click);
            // 
            // pesszimistaToolStripMenuItem
            // 
            this.pesszimistaToolStripMenuItem.Name = "pesszimistaToolStripMenuItem";
            this.pesszimistaToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.pesszimistaToolStripMenuItem.Text = "Pesszimista";
            this.pesszimistaToolStripMenuItem.Click += new System.EventHandler(this.pesszimistaToolStripMenuItem_Click);
            // 
            // regisztracioToolStripMenuItem
            // 
            this.regisztracioToolStripMenuItem.Name = "regisztracioToolStripMenuItem";
            this.regisztracioToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.regisztracioToolStripMenuItem.Text = "Regisztracio";
            this.regisztracioToolStripMenuItem.Click += new System.EventHandler(this.regisztracioToolStripMenuItem_Click);
            // 
            // szamlaToolStripMenuItem
            // 
            this.szamlaToolStripMenuItem.Name = "szamlaToolStripMenuItem";
            this.szamlaToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.szamlaToolStripMenuItem.Text = "Szamla";
            this.szamlaToolStripMenuItem.Click += new System.EventHandler(this.szamlaToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(635, 226);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Kilepes";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(635, 197);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "XML";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 299);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menu1);
            this.MainMenuStrip = this.menu1;
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.menu1.ResumeLayout(false);
            this.menu1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem tervezoCsapatokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem csaladokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem szuresekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tamogatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem konkurenciavezerlesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pesszimistaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regisztracioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem szamlaToolStripMenuItem;
        private System.Windows.Forms.Button button2;
    }
}