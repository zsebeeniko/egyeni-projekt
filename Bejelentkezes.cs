﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8
{

    public partial class Bejelentkezes : Form
    {
        public Bejelentkezes()
        {
            InitializeComponent();
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Chocolate; //a form szine
           // dataGridView1.BackgroundColor = System.Drawing.Color.Beige; //a datagridview-k szine
            button3.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Belepes
            String felnev = "", jelszo = "", jelszo2 = "", kriptalt = "";
            felnev = textBox1.Text.ToString();
            jelszo = textBox2.Text.ToString();
            jelszo2 = textBox3.Text.ToString();

            if (jelszo.Equals(jelszo2, StringComparison.Ordinal) != true)
            {
                DialogResult result2;
                result2 = MessageBox.Show("A ket jelszo nem azonos !");
            }

            string Err = "";
            string ErrM = "";
            bool Err1 = true;
            Regisztracio r = new Regisztracio(ref Err1);

            kriptalt = r.GetMd5Hash(jelszo); //kriptalom a bejelentkezni akaro jelszavat

            int fel_tipus = r.letezike2(felnev, kriptalt);
            
            Globalis.var = fel_tipus;
            
            if (fel_tipus != 0)
            {
                Menu form = new Menu();
                form.Show();
//                this.Close();
            }
            else 
            {
                DialogResult result2;
                result2 = MessageBox.Show("Ervenytelen felhasznalonev vagy jelszo !");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Regisztracio
            
                Form8 b = new Form8();
                b.Show();
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Kilepes
            this.Close();
        }
    }
}
