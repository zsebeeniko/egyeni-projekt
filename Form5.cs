﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Lab8
{
    public partial class Form5 : Form
    {
        public int valtozo;
        public int valtozo2;
        public bool Err;
        public bool First;
        public string errmess;
        public bool error;
        public string valaszt;
        public ArrayList al;
        TCsapat a = new TCsapat();
        bool filter = false;

        public ArrayList al1;
        Tamogatok a1 = new Tamogatok();
        System.Data.DataSet ds;

        public Form5()
        {
            InitializeComponent();
        }
        private void Form5_Load(object sender, EventArgs e)
        {
            this.BackColor = System.Drawing.Color.Chocolate;
            dataGridView1.BackgroundColor = System.Drawing.Color.Beige;
            First = true;
            al = new ArrayList();
            al = a.Feltolt();

            foreach (string c in al)
            {
                comboBox1.Items.Add(c);
            }
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast


            al1 = new ArrayList();
            al1 = a1.Feltolt();

            foreach (string c in al1)
            {
                comboBox2.Items.Add(c);
            }
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList; //letiltja a ComboBox-ba valo beirast

            Tamogatok2 b = new Tamogatok2(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds(ref ErrM);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Tamogat";
            if (Globalis.var == 3)
            {
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                button6.Enabled = false;
            }
         
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filter == true)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Tamogatok2 b = new Tamogatok2(ref Err);

                string ErrM = "";
                ds = b.kiolvas_ds(ref ErrM);
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Tamogat";
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (filter == true)
            {
                if (!First)
                {
                    dataGridView1.DataSource = null;
                }
                else
                {
                    First = false;
                }
                Tamogatok2 b = new Tamogatok2(ref Err);
                string ErrM = "";
                ds = b.kiolvas_ds1(comboBox1.Text, ref ErrM);
                dataGridView1.DataSource = ds;
                dataGridView1.DataMember = "Tamogat";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void button4_Click(object sender, EventArgs e)
        {
            //uj gomb
            filter = false;
            valaszt = "uj";
            textBox1.Enabled = true;
            textBox1.Text = "";
            textBox2.Enabled = true;
            textBox2.Text = "";
            comboBox1.SelectedItem = null;
            comboBox2.SelectedItem = null;
            this.Uj();
            valtozo = -1;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //modosit
            filter = false;
            try
            {
                DataGridViewRow sor1 = dataGridView1.SelectedRows[0];
                string value1;
                string value2;
                string value3;
                string value4;
              
                valtozo = Convert.ToInt32(sor1.Cells[0].Value);
                valtozo2 = Convert.ToInt32(sor1.Cells[1].Value);
                value1 = sor1.Cells[2].Value.ToString();
                value2 = sor1.Cells[4].Value.ToString();
                value3 = sor1.Cells[3].Value.ToString();
                value4 = sor1.Cells[5].Value.ToString();
                
                textBox1.Text = value2;
                textBox2.Text = value4;
                comboBox1.SelectedItem = value1;
                comboBox2.SelectedItem = value3;

                button6.Enabled = true;
                button5.Enabled = true;
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                // this.Uj();

            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
        }
        private void Uj()
        {
            button6.Enabled = true;
            button5.Enabled = true;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            button1.Enabled = false;
        }

        private void Kezdet()
        {
            button6.Enabled = false;
            button5.Enabled = false;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            button1.Enabled = true;
        }

        private CurrencyManager CM
        {
            get
            {
                return this.dataGridView1.BindingContext[this.dataGridView1.DataSource, this.dataGridView1.DataMember] as CurrencyManager;
            }
        }


        private DataGridViewRow CurrentRow
        {
            get
            {
                if (this.CM == null) return null;
                if (this.CM.Position == -1) return null;
                return dataGridView1.SelectedRows[0];
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //torol
            DialogResult result;
            result = MessageBox.Show("Biztos ki akarja torolni az elemet", "", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    SqlConnection conn = new SqlConnection();
                    conn.ConnectionString = "Data Source=(local);Initial Catalog=projekt;Integrated Security=SSPI";
                    conn.Open();

                    DataGridViewRow item = dataGridView1.SelectedRows[0];

                    string queryDeleteString = ("DELETE FROM Tamogat WHERE CsapatID =" + Convert.ToInt32(item.Cells[0].Value) + "and TamogatoID ="+Convert.ToInt32(item.Cells[1].Value));
                    SqlCommand sqlDelete = new SqlCommand();
                    sqlDelete.CommandText = queryDeleteString;
                    sqlDelete.Connection = conn;
                    sqlDelete.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    String Errm = "";
                    Errm = ex.Message;
                }
            }
            
            Tamogatok2 c = new Tamogatok2(ref Err);
            string ErrM1 = "";
            ds = c.kiolvas_ds(ref ErrM1);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Tamogat";
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //megse
            this.Kezdet();
            comboBox1.SelectedIndex = -1;
            comboBox2.SelectedIndex = -1;
            textBox1.Text = "";
            textBox2.Text = "";
            this.Refresh();
            dataGridView1.Refresh();
            Tamogatok2 b = new Tamogatok2(ref Err);
            string ErrM = "";
            ds = b.kiolvas_ds(ref ErrM);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Tamogat";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //textbox1
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //textbox2
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //mentes
            try
            {
               
                if (valtozo == -1)
                {//uj beszurasa
                    string ss1 = "", ss2 = "";
                    int szam = 0;
                    if (comboBox2.SelectedItem != null)
                    ss1 = comboBox2.SelectedItem.ToString(); // Beszallitok
                    else
                    {
                        szam = szam + 1;
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    if (comboBox1.SelectedItem != null)
                     ss2 = comboBox1.SelectedItem.ToString(); // Partnerek
                    else
                    {
                        szam = szam + 1;
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    string ss3 = textBox1.Text.ToString();    // CsapatNev
                    int ss4 = Convert.ToInt32(textBox2.Text);

                    string Err = "";
                    bool Err1 = true;
                    Tamogatok2 a3 = new Tamogatok2(ref Err1);
                   
                    a3.beszuras(ss2, ss1, ss3, ss4);
                }
                else
                {//modositas
                    string s1 = "", s2 = "";
                    if (comboBox2.SelectedItem != null)
                    s1 = comboBox2.SelectedItem.ToString();// Beszallitok
                    else
                    {
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    if (comboBox1.SelectedItem != null)
                     s2 = comboBox1.SelectedItem.ToString();// Partnerek
                    else
                    {
                        DialogResult result2;
                        result2 = MessageBox.Show("Hianyos adatok!!! Toltse ki az osszes mezot!!!");
                    }
                    string s3 = textBox1.Text.ToString();    //CsapatNev
                    int s4 = Convert.ToInt32(textBox2.Text);

                    Tamogatok2 a2 = new Tamogatok2(ref Err);
                    a2.modosit(valtozo, valtozo2, s2, s1, s3, s4);

                    Tamogatok2 b = new Tamogatok2(ref Err);
                    string ErrM = "";
                    ds = b.kiolvas_ds(ref ErrM);

                    dataGridView1.DataSource = ds;
                    dataGridView1.DataMember = "Tamogat";
                }
            }
            catch (Exception ex)
            {
                String Errm = "";
                Errm = ex.Message;
            }
            dataGridView1.Refresh();
            Tamogatok2 c = new Tamogatok2(ref Err);
            string ErrM1 = "";
            ds = c.kiolvas_ds(ref ErrM1);
            dataGridView1.DataSource = ds;
            dataGridView1.DataMember = "Tamogat";
            this.Refresh();
        }
    }
}
