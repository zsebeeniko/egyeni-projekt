﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Windows.Forms;
namespace Lab8
{
    public struct OneClass8
    {
        public int tamogatoid;
        public int csapatid;
        public int osszeg;
        public string datum;
    }
    class Tamogat_pesszimista : DAL
    {
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();

        private string sQuery;
        public Tamogat_pesszimista(ref bool Err)
        {
            if (!base.IsConnectCreated())
            {
                Err = !base.MakeConnection();
            }
            da = new SqlDataAdapter();
            da.TableMappings.Add("Table", "Tamogat");
        }

        public List<OneClass8> kiolvas()
        {
            List<OneClass8> Tamogat_pesszimista = new List<OneClass8>();
            OneClass8 oc = new OneClass8();
            string error = "";
            SqlDataReader reader = myExecuteReader("SELECT TamogatoID, CsapatID, Osszeg, Datum FROM Tamogat like", ref error);
            while (reader.Read())
            {
                oc.tamogatoid = Convert.ToInt32(reader["TamogatoID"]);
                oc.csapatid = Convert.ToInt32(reader["CsapatID"]);
                oc.osszeg = Convert.ToInt32(reader["Osszeg"]);
                oc.datum = reader["Datum"].ToString();
                Tamogat_pesszimista.Add(oc);
            }
            return Tamogat_pesszimista;
        }
        public DataSet kiolvas_ds(ref string Errm)
        {
            string s = "SELECT TamogatoID, CsapatID, Osszeg, Datum FROM Tamogat";
            ds = base.myFillDataSetFromString(s, "Tamogat", ref da, ref Errm);
            return ds;
        }

        public int osszeg(ref string Err, int tamogat_id, int csapat_id)
        {
            //biztos, hogy egyedi lesz a kulcs, mert a letezo legnagyobbat keressuk meg es eggyel nagyobb lesz az uj
            //kesobb nem kell tesztelni
            sQuery = "SELECT Osszeg from Tamogat where TamogatoID = " + tamogat_id + " and CsapatID = " + csapat_id;
            int suly = base.myExecuteScalar(sQuery, ref Err);
            return suly;
        }



        public void modosit(int ID, int suly)
        {
            OpenConnection();
            string s = "UPDATE Betegsegek SET Sulyossag = " + suly + " WHERE BetegsegID = " + ID;
            base.myInsertCommand(s);
            CloseConnection();
        }

        public void Tranzakcio1(string connectionString, int tamogatoid1, int csapatid1, int osszeg1, int tamogatoid2, int csapatid2, int osszeg2)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction(IsolationLevel.Serializable);

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;
                                
                try
                {
                    command.CommandText =
                        "UPDATE Tamogat set Osszeg = " + osszeg1 + "WHERE TamogatoID =" + tamogatoid1 + " and CsapatID =" + csapatid1;
                    command.ExecuteNonQuery();
                    System.Threading.Thread.Sleep(10000);
                    command.CommandText =
                        "UPDATE Tamogat set Osszeg = " + osszeg2 + "WHERE TamogatoID =" + tamogatoid2 + " and CsapatID =" + csapatid2;
                    command.ExecuteNonQuery();
                    // Attempt to commit the transaction.
                    transaction.Commit();
                    Console.WriteLine("Sikeresen modositva!!!");
                }
                catch (Exception EX)
                {
                    Console.WriteLine("Commit Exception Type: {0}", EX.GetType());
                    Console.WriteLine("  Message: {0}", EX.Message);

                    // Attempt to roll back the transaction. 
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // This catch block will handle any errors that may have occurred 
                        // on the server that would cause the rollback to fail, such as 
                        // a closed connection.
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                }
            }
        }
        public void Tranzakcio2(string connectionString, int tamogatoid1, int csapatid1, int osszeg1, int tamogatoid2, int csapatid2, int osszeg2)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction(IsolationLevel.Serializable);

                // Must assign both transaction object and connection 
                // to Command object for a pending local transaction
                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    command.CommandText =
                        "UPDATE Tamogat set Osszeg = " + osszeg1 + "WHERE TamogatoID =" + tamogatoid1 + " and CsapatID =" + csapatid1;
                    command.ExecuteNonQuery();
                    System.Threading.Thread.Sleep(10000);
                    command.CommandText =
                       "UPDATE Tamogat set Osszeg = " + osszeg2 + "WHERE TamogatoID =" + tamogatoid2 + " and CsapatID =" + csapatid2;
                    command.ExecuteNonQuery();
                    // Attempt to commit the transaction.
                    transaction.Commit();
                    Console.WriteLine("Sikeresen modositva!!!");
                }
                catch (Exception EX)
                {
                    Console.WriteLine("Commit Exception Type: {0}", EX.GetType());
                    Console.WriteLine("  Message: {0}", EX.Message);

                    // Attempt to roll back the transaction. 
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        // This catch block will handle any errors that may have occurred 
                        // on the server that would cause the rollback to fail, such as 
                        // a closed connection.
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }
                }
            }
        }
    }
}
